/*=======================================================//
// dog.QC - CustomTF 3.2.OfN              - 02/01/2023 - //
// by Sergio Fuma�a Grunwaldt - OfteN [cp]               //
=========================================================//
 This QuakeC file contains all the dog monster functions
---------------------------------------------------------//
 I added this for the "frontlin.bsp" map mainly...
=========================================================*/

// Default settings
#define DOG_DEFAULT_LIVES  10
#define DOG_DEFAULT_HP    400
#define DOG_DEFAULT_MINR  120 // Minimum respawn delay time (default)
#define DOG_DEFAULT_MAXR  150 // Maximum respawn delay time (default)

// Damages for jump attack
#define DOG_JUMPDMG_MIN    40
#define DOG_JUMPDMG_MAX    80
// Damages for bites from dog
#define DOG_BITEDMG_MIN    40
#define DOG_BITEDMG_MAX    80

/*===============================================================================================

EXPLANATION OF HOW THE ENTITY FIELDS ARE USED (thnx? np.. :P)
---------------------------------------------

For a dog monster entity:
-------------------------

.lives          - Amount of times for it to respawn
.ammo_shells    - Minimum delay for respawn time
.ammo_nails     - Maximum delay for respawn time
.pos1           - Vector holding original spawning origin, for relocate in the same original place on respawn
.oldorigin      - Vector holding original spawning angles, for orienting in the same original direction on respawn

================================================================================================*/

// Forward declarations
void() DogCheckRespawn;

/*
==============================================================================

DOG

==============================================================================
*/
$cd id1/models/dog
$origin 0 0 24
$base base
$skin skin

$frame attack1 attack2 attack3 attack4 attack5 attack6 attack7 attack8

$frame death1 death2 death3 death4 death5 death6 death7 death8 death9

$frame deathb1 deathb2 deathb3 deathb4 deathb5 deathb6 deathb7 deathb8
$frame deathb9

$frame pain1 pain2 pain3 pain4 pain5 pain6

$frame painb1 painb2 painb3 painb4 painb5 painb6 painb7 painb8 painb9 painb10
$frame painb11 painb12 painb13 painb14 painb15 painb16

$frame run1 run2 run3 run4 run5 run6 run7 run8 run9 run10 run11 run12

$frame leap1 leap2 leap3 leap4 leap5 leap6 leap7 leap8 leap9

$frame stand1 stand2 stand3 stand4 stand5 stand6 stand7 stand8 stand9

$frame walk1 walk2 walk3 walk4 walk5 walk6 walk7 walk8


void() dog_leap1;
void() dog_run1;

/*
================
dog_bite

================
*/
void() dog_bite =
{
	local vector	delta;
	local float 	ldmg;

	if (!self.enemy)
		return;

	ai_charge(10);

	if (!CanDamage (self.enemy, self))
		return;

	delta = self.enemy.origin - self.origin;

	if (vlen(delta) > 100)
		return;
		
	ldmg = RandomRange(#DOG_BITEDMG_MIN, #DOG_BITEDMG_MAX);//(random() + random() + random()) * 8;
	TF_T_Damage (self.enemy, self, self, ldmg, #TF_TD_DIRECT | #TF_TD_DONTGIB, #TF_TD_MELEE);

	ldmg = RandomRange(5, 11);
	SpawnBloodEx(self.enemy.origin, 0.5, ldmg);
};

void()	Dog_JumpTouch =
{
	local	float	ldmg;

	if (self.health <= 0)
		return;
		
	if (other.takedamage)
	{
		if ( vlen(self.velocity) > 300 )
		{
			ldmg = RandomRange(#DOG_JUMPDMG_MIN, #DOG_JUMPDMG_MAX);//10 + 10*random();
			TF_T_Damage (other, self, self, ldmg, #TF_TD_DIRECT | #TF_TD_DONTGIB, #TF_TD_MELEE);	

			ldmg = RandomRange(5, 11);
			SpawnBloodEx(other.origin, 0.5, ldmg);
		}
	}

	if (!checkbottom(self))
	{
		if (self.flags & #FL_ONGROUND)
		{	// jump randomly to not get hung up
//dprint ("popjump\n");
	//self.touch = SUB_Null;
	self.touch = MonsterTouch;
	self.think = dog_leap1;
	self.nextthink = time + 0.1;

//			self.velocity_x = (random() - 0.5) * 600;
//			self.velocity_y = (random() - 0.5) * 600;
//			self.velocity_z = 200;
//			self.flags = self.flags - FL_ONGROUND;
		}
		return;	// not on ground yet
	}

	//self.touch = SUB_Null;
	self.touch = MonsterTouch;
	self.think = dog_run1;
	self.nextthink = time + 0.1;
};

void() dog_stand1	=[	$stand1,	dog_stand2	] {ai_stand();};
void() dog_stand2	=[	$stand2,	dog_stand3	] {ai_stand();};
void() dog_stand3	=[	$stand3,	dog_stand4	] {ai_stand();};
void() dog_stand4	=[	$stand4,	dog_stand5	] {ai_stand();};
void() dog_stand5	=[	$stand5,	dog_stand6	] {ai_stand();};
void() dog_stand6	=[	$stand6,	dog_stand7	] {ai_stand();};
void() dog_stand7	=[	$stand7,	dog_stand8	] {ai_stand();};
void() dog_stand8	=[	$stand8,	dog_stand9	] {ai_stand();};
void() dog_stand9	=[	$stand9,	dog_stand1	] {ai_stand();};

void() dog_walk1	=[	$walk1 ,	dog_walk2	] {
if (random() < 0.1) // Original was < 0.2
	sound (self, #CHAN_VOICE, "dog/idle.wav", 1, #ATTN_IDLE);
ai_walk(8);};
void() dog_walk2	=[	$walk2 ,	dog_walk3	] {ai_walk(8);};
void() dog_walk3	=[	$walk3 ,	dog_walk4	] {ai_walk(8);};
void() dog_walk4	=[	$walk4 ,	dog_walk5	] {ai_walk(8);};
void() dog_walk5	=[	$walk5 ,	dog_walk6	] {ai_walk(8);};
void() dog_walk6	=[	$walk6 ,	dog_walk7	] {ai_walk(8);};
void() dog_walk7	=[	$walk7 ,	dog_walk8	] {ai_walk(8);};
void() dog_walk8	=[	$walk8 ,	dog_walk1	] {ai_walk(8);};

void() dog_run1		=[	$run1  ,	dog_run2	] {
if (random() < 0.2)
	sound (self, #CHAN_VOICE, "dog/idle.wav", 1, #ATTN_IDLE);
ai_run(16);};
void() dog_run2		=[	$run2  ,	dog_run3	] {ai_run(32);};
void() dog_run3		=[	$run3  ,	dog_run4	] {ai_run(32);};
void() dog_run4		=[	$run4  ,	dog_run5	] {ai_run(20);};
void() dog_run5		=[	$run5  ,	dog_run6	] {ai_run(64);};
void() dog_run6		=[	$run6  ,	dog_run7	] {ai_run(32);};
void() dog_run7		=[	$run7  ,	dog_run8	] {ai_run(16);};
void() dog_run8		=[	$run8  ,	dog_run9	] {ai_run(32);};
void() dog_run9		=[	$run9  ,	dog_run10	] {ai_run(32);};
void() dog_run10	=[	$run10  ,	dog_run11	] {ai_run(20);};
void() dog_run11	=[	$run11  ,	dog_run12	] {ai_run(64);};
void() dog_run12	=[	$run12  ,	dog_run1	] {ai_run(32);};

void() dog_atta1	=[	$attack1,	dog_atta2	] {ai_charge(10);};
void() dog_atta2	=[	$attack2,	dog_atta3	] {ai_charge(10);};
void() dog_atta3	=[	$attack3,	dog_atta4	] {ai_charge(10);};
void() dog_atta4	=[	$attack4,	dog_atta5	] {
sound (self, #CHAN_VOICE, "dog/dattack1.wav", 1, #ATTN_NORM);
MonsterAuraPower();
dog_bite();};
void() dog_atta5	=[	$attack5,	dog_atta6	] {ai_charge(10);};
void() dog_atta6	=[	$attack6,	dog_atta7	] {ai_charge(10);};
void() dog_atta7	=[	$attack7,	dog_atta8	] {ai_charge(10);};
void() dog_atta8	=[	$attack8,	dog_run1	] {ai_charge(10);};

void() dog_leap1	=[	$leap1,		dog_leap2	] {ai_face();};
void() dog_leap2	=[	$leap2,		dog_leap3	]
{
	ai_face();
	
	self.touch = Dog_JumpTouch;
	makevectors (self.angles);
	self.origin_z = self.origin_z + 1;
	self.velocity = v_forward * 300 + '0 0 200';
	if (self.flags & #FL_ONGROUND)
		self.flags = self.flags - #FL_ONGROUND;

	MonsterAuraPower();
};

void() dog_leap3	=[	$leap3,		dog_leap4	] {};
void() dog_leap4	=[	$leap4,		dog_leap5	] {};
void() dog_leap5	=[	$leap5,		dog_leap6	] {};
void() dog_leap6	=[	$leap6,		dog_leap7	] {};
void() dog_leap7	=[	$leap7,		dog_leap8	] {};
void() dog_leap8	=[	$leap8,		dog_leap9	] {};
void() dog_leap9	=[	$leap9,		dog_leap9	] {};

void() dog_pain1	=[	$pain1 ,	dog_pain2	] {};
void() dog_pain2	=[	$pain2 ,	dog_pain3	] {};
void() dog_pain3	=[	$pain3 ,	dog_pain4	] {};
void() dog_pain4	=[	$pain4 ,	dog_pain5	] {};
void() dog_pain5	=[	$pain5 ,	dog_pain6	] {};
void() dog_pain6	=[	$pain6 ,	dog_run1	] {};

void() dog_painb1	=[	$painb1 ,	dog_painb2	] {};
void() dog_painb2	=[	$painb2 ,	dog_painb3	] {};
void() dog_painb3	=[	$painb3 ,	dog_painb4	] {ai_pain(4);};
void() dog_painb4	=[	$painb4 ,	dog_painb5	] {ai_pain(12);};
void() dog_painb5	=[	$painb5 ,	dog_painb6	] {ai_pain(12);};
void() dog_painb6	=[	$painb6 ,	dog_painb7	] {ai_pain(2);};
void() dog_painb7	=[	$painb7 ,	dog_painb8	] {};
void() dog_painb8	=[	$painb8 ,	dog_painb9	] {ai_pain(4);};
void() dog_painb9	=[	$painb9 ,	dog_painb10	] {};
void() dog_painb10	=[	$painb10 ,	dog_painb11	] {ai_pain(10);};
void() dog_painb11	=[	$painb11 ,	dog_painb12	] {};
void() dog_painb12	=[	$painb12 ,	dog_painb13	] {};
void() dog_painb13	=[	$painb13 ,	dog_painb14	] {};
void() dog_painb14	=[	$painb14 ,	dog_painb15	] {};
void() dog_painb15	=[	$painb15 ,	dog_painb16	] {};
void() dog_painb16	=[	$painb16 ,	dog_run1	] {};

void(entity attacker, float damage) dog_pain =
{
	if (self.health <= 0)
        return;

	AI_CheckAttacker(attacker);

    if (random()*80 > damage) // was 60
		return;		// didn't flinch

	sound (self, #CHAN_VOICE, "dog/dpain1.wav", 1, #ATTN_NORM);

	if (random() > 0.5)
		dog_pain1 ();
	else
		dog_painb1 ();
};

void() dog_die1		=[	$death1,	dog_die2	] {};
void() dog_die2		=[	$death2,	dog_die3	] {};
void() dog_die3		=[	$death3,	dog_die4	] {};
void() dog_die4		=[	$death4,	dog_die5	] {};
void() dog_die5		=[	$death5,	dog_die6	] {};
void() dog_die6		=[	$death6,	dog_die7	] {};
void() dog_die7		=[	$death7,	dog_die8	] {};
void() dog_die8		=[	$death8,	dog_die9	] {};
void() dog_die9		=[	$death9,	dog_die9	] {MonsterCorpse();};

void() dog_dieb1		=[	$deathb1,	dog_dieb2	] {};
void() dog_dieb2		=[	$deathb2,	dog_dieb3	] {};
void() dog_dieb3		=[	$deathb3,	dog_dieb4	] {};
void() dog_dieb4		=[	$deathb4,	dog_dieb5	] {};
void() dog_dieb5		=[	$deathb5,	dog_dieb6	] {};
void() dog_dieb6		=[	$deathb6,	dog_dieb7	] {};
void() dog_dieb7		=[	$deathb7,	dog_dieb8	] {};
void() dog_dieb8		=[	$deathb8,	dog_dieb9	] {};
void() dog_dieb9		=[	$deathb9,	dog_dieb9	] {MonsterCorpse();};

float() dog_die =
{
	self.enemy = world; // messes if this weren't here...
	self.is_connected = #FALSE;
	self.tfstate = self.tfstate - (self.tfstate & #TFSTATE_INFECTED);

// check for gib
	if (self.health < #DOG_GIB_BELOWTHIS)
	{
		sound (self, #CHAN_VOICE + #NOPHS_ADD_MONSTERDIE, "player/udeath.wav", 1, #ATTN_MONSTERDIE);
		ThrowGib ("progs/gib3.mdl", self.health,#TRUE,0,#GIB3_KGS, #FALSE);
		ThrowGib ("progs/gib3.mdl", self.health,#TRUE,0,#GIB3_KGS, #FALSE);
		ThrowGib ("progs/gib3.mdl", self.health,#TRUE,0,#GIB3_KGS, #FALSE);
		ThrowMonsterHead ("progs/h_dog.mdl", self.health);

		GibbedBloodSplash(self, self.health); // March 2023

		//dremove(self);
		SelfNextRange(self.ammo_shells, self.ammo_nails);
		self.think = DogCheckRespawn;
		self.modelindex = modelindex_null;
		self.#monsterflag = "";//#STRFLAG_MONSTER

		self.movetype = #MOVETYPE_NONE;
		self.solid = #SOLID_NOT;
		return #THDIE_GIBBED;
	}

// regular death
	sound (self, #CHAN_VOICE + #NOPHS_ADD_MONSTERDIE, "dog/ddeath.wav", 1, #ATTN_MONSTERDIE);
	self.solid = #SOLID_NOT;

	if (random() > 0.5)
		dog_die1 ();
	else
		dog_dieb1 ();

	MonsterDead();

	return #THDIE_CORPSE;
};

//============================================================================

/*
==============
CheckDogMelee

Returns TRUE if a melee attack would hit right now
==============
*/
float()	CheckDogMelee =
{
	if (enemy_range == #RANGE_MELEE)
	{	// FIXME: check canreach
		self.attack_state = #AS_MELEE;
		return #TRUE;
	}
	return #FALSE;
};

/*
==============
CheckDogJump

==============
*/
float()	CheckDogJump =
{
	local	vector	dist;
	local	float	d;

	if (self.origin_z + self.mins_z > self.enemy.origin_z + self.enemy.mins_z
	+ 0.75 * self.enemy.size_z)
		return #FALSE;
		
	if (self.origin_z + self.maxs_z < self.enemy.origin_z + self.enemy.mins_z
	+ 0.25 * self.enemy.size_z)
		return #FALSE;
		
	dist = self.enemy.origin - self.origin;
	dist_z = 0;
	
	d = vlen(dist);
	
	if (d < 80)
		return #FALSE;
		
	if (d > 150)
		return #FALSE;
		
	return #TRUE;
};

float()	DogCheckAttack =
{
	// if close enough for slashing, go for it
	if (CheckDogMelee ())
	{
		self.attack_state = #AS_MELEE;
		return #TRUE;
	}
	
	if (CheckDogJump ())
	{
		self.attack_state = #AS_MISSILE;
		return #TRUE;
	}
	
	return #FALSE;
};

//======================================================================================================
// Returns an appropriate dog name, the way I implemented it, same dog names for the same maps always

#define NUM_DOGNAMES 16

float DogNameBits;

float() GetUnusedDogNameBit =
{
	local float num;

	num = 1;

	while (num <= #NUM_DOGNAMES)
	{
		if (!(DogNameBits & itob(num)))
		{
			DogNameBits = DogNameBits | itob(num);
			return num;
		}

		num = num + 1;
	}

	// Reusing local float - The following randomization only happens when no dog names left...
	num = 1 + floor((#NUM_DOGNAMES * random()) - 0.001);

	return maxvalue(num, 1); 
};

string(float namebit) GetDogNameString =
{
	if (namebit == 1)
		return "Howler";
	else if (namebit == 2)
		return "Blaze";
	else if (namebit == 3)
		return "Sabre";
	else if (namebit == 4)		
		return "Fang";
	else if (namebit == 5)
		return "Spike";
	else if (namebit == 6)
		return "Chopper";
	else if (namebit == 7)
		return "Mike";	
	else if (namebit == 8)
		return "Tod";
	else if (namebit == 9)
		return "Peggie";
	else if (namebit == 10)
		return "Rhon";
	else if (namebit == 11)
		return "Flop";
	else if (namebit == 12)
		return "Skippy";
	else if (namebit == 13)
		return "Tobby";
	else if (namebit == 14)
		return "Max";
	else if (namebit == 15)
		return "Butter";
	else
		return "Flippy";
};

string() GetDogName =
{
	local float bit;

	bit = GetUnusedDogNameBit();

	return GetDogNameString(bit);
};

//=============================================================================================================
// November 2024 - Do a trace reaching ground below and update origin on self

void() DOG_FixBottom =
{
	tracebox(self.origin, self.mins, self.maxs, self.origin - '0 0 8192', #FALSE, self);

	setorigin(self, trace_endpos + '0 0 24.1');
};

//===========================================================================

/*QUAKED monster_dog (1 0 0) (-32 -32 -24) (32 32 40) Ambush

*/
#ifdef DOG_INCLUDED
void() monster_dog =
{
	if (self.team_no != 1 && self.team_no != 2 && self.team_no != 3 && self.team_no != 4)
	{
		RPrint("Map bug: Dog entity without team_no set to a valid team!\n");
		dremove(self);
		return;
	}
	
	precache_model ("progs/h_dog.mdl");
	precache_model ("progs/dog.mdl");

	precache_sound ("dog/dattack1.wav");
	precache_sound ("dog/ddeath.wav");
	precache_sound ("dog/dpain1.wav");
	precache_sound ("dog/dsight.wav");
	precache_sound ("dog/idle.wav");

	self.solid = #SOLID_SLIDEBOX;
	self.movetype = #MOVETYPE_STEP;

	setmodel (self, "progs/dog.mdl");

	setsize (self, '-32 -32 -24', '32 32 40');
	
	if (!self.health)
		self.health = #DOG_DEFAULT_HP;

	self.max_health = self.health;

	if (!self.lives)
		self.lives = #DOG_DEFAULT_LIVES;

	if (!self.ammo_shells)
		self.ammo_shells = #DOG_DEFAULT_MINR;
	if (!self.ammo_nails)
		self.ammo_nails = #DOG_DEFAULT_MAXR;

	self.netname = GetDogName();

	self.#monster_type = #MONSTER_DOG;

	self.real_owner = self;

	self.yaw_speed = #DOG_YAWSPEED;

	// November 2024
	DOG_FixBottom();
	// End November 2024

	self.#monsterflag = #STRFLAG_MONSTER; // flag to identify monsters/army for sentry targetting, among other things

	self.th_stand = dog_stand1;
	self.th_walk = dog_walk1;
	self.th_run = dog_run1;
	self.th_pain = dog_pain;
	self.th_die = dog_die;
	self.th_melee = dog_atta1;
	self.th_missile = dog_leap1;

	self.touch = MonsterTouch;

	walkmonster_start();

	self.is_connected = #TRUE;
};
#endif

//===============================================================================================================
// Dog did become a corpse

void() DogCheckRespawn =
{
	if (self.lives <= 1) // If we ran out of lives...
	{
		dremove(self); // ...remove entity entirely
		return;
	}

	self.flags = self.flags - (self.flags & #FL_FINDABLE_NONSOLID);
	
	self.solid = #SOLID_SLIDEBOX;
	self.movetype = #MOVETYPE_STEP;

	// Commented out the following line in January 2023, because killing a dog sets DAMAGE_NO, which impides T_Damage to run pain, thus the following comment is wrong...
	//self.takedamage = #DAMAGE_AIM; // placed here as safety cause AI_CheckAttacker or pain function may lead to let this go as DAMAGE_NO, (without the walkmonster_start_go ran)

	setmodel (self, "progs/dog.mdl");

	setsize (self, '-32 -32 -24', '32 32 40');
	
	self.health = self.max_health;

	self.lives = self.lives - 1;

	setorigin(self, self.pos1);
	self.angles = self.oldorigin;

	self.#monsterflag = #STRFLAG_MONSTER; // flag to identify monsters/army for sentry targetting, among other things

	self.th_stand = dog_stand1;
	self.th_walk = dog_walk1;
	self.th_run = dog_run1;
	self.th_pain = dog_pain;
	self.th_die = dog_die;
	self.th_melee = dog_atta1;
	self.th_missile = dog_leap1;

	// NOT NEEDED - .pos1 is set to old origin on walkmonster_start_go() DOG_FixBottom(); // November 2024
	
	SpawnTeleFX(self, #TELEFX_RNDSOUND); // with random teleportation sound

	spawn_tdeath(self.origin, self); // telefrag stuff on respawning spot

	walkmonster_start();

	self.frame = $stand1;

	self.touch = MonsterTouch;

	self.is_connected = #TRUE;

	// Notify everyone on our team about the dog respawn
	teamprefixsprint(self.team_no,world);
	teamsprint7(self.team_no,"Our precious dog ",self.netname," is back to service on the battlefield!\n","","","","");
};

void() DogCorpse =
{
	self.classname = "monster_dog";
	SelfNextRange(self.ammo_shells, self.ammo_nails);
	self.think = DogCheckRespawn;
};

//===============================================================================================================
// Added to lookaround() to detect spies and thieves for dogs

entity(entity dog) DogSpecialLookaroundAdd =
{
	local entity te, oself;

	if (ceasefire)
		return world;

	oself = self;
	self = dog;

	te = find(world, classname, "player");

	while (te)
	{
		if (te.health > 0)
		if (te.takedamage)
		if (!Teammate(te.team_no, self.team_no))
		if (!te.is_feigning)
		if (!te.is_cameraviewing)
		if (te.playerclass != #PC_UNDEFINED)
		if (!(te.done_custom & #CUSTOM_BUILDING))
		if (visible(te))
		{
			if (HasJobAndBits(te, #JOB_THIEF, #JOB_ACTIVE | #JOB_FULL_HIDE))
			{
				self = oself;
				return te;
			}

			if (Teammate(te.undercover_team, self.team_no))
			{
				self = oself;
				return te;
			}
		}

		te = find(te, classname, "player");
	}

	self = oself;

	return world;
};

//===============================================================================================================
// Obituary stuff for the doggies

void(entity targ, entity attacker) DogKilledSomething =
{
	local float teammate;
	local string st;

	teammate = #FALSE;

	if (Teammate(targ.team_no, attacker.team_no))
		teammate = #TRUE;
	
	if (targ.classname == "monster_dog")
	{
		if (!teammate)
			bprint(#PRINT_MEDIUM, "The hound ",attacker.netname," shows to its species what is more rabid by killing the enemy dog ", targ.netname,"\n");
		else
			bprint(#PRINT_MEDIUM, "The hound ",attacker.netname," shows to its species what is more rabid by killing the friendly dog ", targ.netname,"\n");
		return;
	}

	if (targ.classname == "player")
	{
		if (!teammate)
			bprint(#PRINT_MEDIUM, "The dog ", attacker.netname, " bites ", targ.netname, " hard!\n");
		else
			bprint(#PRINT_MEDIUM, "The dog ", attacker.netname, " sadly teamkills ", targ.netname, "\n");
		return;
	}

	if (IsBuildingEx(targ))
	{
		st = GetBuildingName(targ);

		if (!teammate)
		{
			if (targ.classname != "monster_turret")
				bprint(#PRINT_MEDIUM, "The dog ", attacker.netname, " destroys ", targ.real_owner.netname, "'s ", st, "\n");
			else
				bprint(#PRINT_MEDIUM, "The dog ", attacker.netname, " destroys a ", st, "\n");
		}
		else
		{
			if (targ.classname != "monster_turret")
				bprint(#PRINT_MEDIUM, "The dog ", attacker.netname, " destroys ", targ.real_owner.netname, "'s friendly ", st, "\n");
			else
				bprint(#PRINT_MEDIUM, "The dog ", attacker.netname, " destroys a friendly ", st, "\n");
		}
		return;
	}

	if (IsMonster(targ))
	{
		st = GetMonsterName(targ);

		if (!teammate)
			bprint(#PRINT_MEDIUM, "The dog ", attacker.netname, " kills ", targ.real_owner.netname, "'s ", st ,"\n");
		else
			bprint(#PRINT_MEDIUM, "The dog ", attacker.netname, " sadly teamkills ", targ.real_owner.netname, "'s ", st, "\n");
		return;
	}
};

void(entity targ, entity attacker) DogFraggedBySomething =
{
	local float teammate;
	local string st;

	teammate = #FALSE;

	if (Teammate(targ.team_no, attacker.team_no))
		teammate = #TRUE;
	
	if (attacker.classname == "player")
	{
		if (!teammate)
		{
			bprint(#PRINT_MEDIUM, attacker.netname, " returns the hound ", targ.netname, " back to the kennel\n");
			Give_Frags_Out(attacker, targ, 1, 0, 1, 1, attacker, targ);
			MonsterKill(attacker);
		}
		else
		{
			bprint(#PRINT_MEDIUM, attacker.netname, " sends the friendly dog ", targ.netname, " to hell\n");
			Give_Frags_Out(attacker, targ, -1, 0, 1, 1, attacker, targ);
		}
		return;
	}

	if (IsBuildingEx(attacker))
	{
		st = GetBuildingName(attacker);

		if (!teammate)
		{			
			if (attacker.classname != "monster_turret")
			{				
				bprint(#PRINT_MEDIUM, attacker.real_owner.netname, "'s ", st, " kills the hound ", targ.netname, "\n");
				Give_Frags_Out(attacker.real_owner, targ, 1, 0, 1, 1, attacker, targ);
			}
			else
				bprint(#PRINT_MEDIUM, st, " kills the hound ", targ.netname, "\n");

			if (attacker.classname == "building_sentrygun" || attacker.classname == "building_tesla" || attacker.classname == "building_miniturret")
				Give_Frags_Out(attacker, targ, 1, 0, 2, 0, attacker, targ); //gun
		}
		else
		{			
			if (attacker.classname != "monster_turret")
			{
				bprint(#PRINT_MEDIUM, attacker.real_owner.netname, "'s ", st, " kills the friendly dog ", targ.netname, "\n");
				Give_Frags_Out(attacker.real_owner, targ, -1, 0, 1, 1, attacker, targ);
			}
			else
				bprint(#PRINT_MEDIUM, st, " kills the friendly dog ", targ.netname, "\n");
		}
		return;
	}

	if (IsMonsterSpecial(attacker, #TRUE)) // Note on the following code block: no check for monster_dog to disallow use of real_owner, as this function is overridden by DogKilledSomething() when dog is attacker...
	{
		st = GetMonsterName(attacker);

		if (!teammate)
		{
			bprint(#PRINT_MEDIUM, "The ", st, " ", attacker.netname, " (", attacker.real_owner.netname, ") sends the hound ");
			bprint(#PRINT_MEDIUM, targ.netname, " to hell\n");
			Give_Frags_Out(attacker.real_owner, targ, 1, 0, 1, 1, attacker, targ);
		}
		else
		{
			bprint(#PRINT_MEDIUM, "The ", st, " ", attacker.netname, " (", attacker.real_owner.netname, ") kills the friendly dog ");
			bprint(#PRINT_MEDIUM, targ.netname, "\n");
			Give_Frags_Out(attacker.real_owner, targ, -1, 0, 1, 1, attacker, targ);
		}
		return;
	}

	if (attacker.classname == "teledeath" || attacker.classname == "teledeath2")
	{
		bprint(#PRINT_MEDIUM, "The dog ", targ.netname, " was telefragged by ", attacker.owner.netname, "\n");
		return;
	}

	// Default death
	bprint(#PRINT_MEDIUM, "The dog ", targ.netname, " simply dies\n");
};
