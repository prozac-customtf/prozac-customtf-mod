+---------------------------------------------------------+
| Prozac CustomTF MOD QuakeC source code+tools 03/10/2020 |
+---------------------------------------------------------+

The old NON-fte version of the mod can probably still be
built using the "make.bat" batch file. Although in the
future, (so maybe when you read this) non-fte quakeC mod
support will be dropped. Use the FTE compiler, please.

If you download this source code repository you will have
a Prozac CuTF MOD prepared to compile under Windows.
It includes the compiled tools to do so, the Quake-C
compiler and preprocessor. To build the MOD normally just
run "makefteqc.bat". If it compiles successfuly, it will
create a file called "fteprogs.dat" on the parent folder
in which make file was run. It's usual to have this source
on something like a fortress/source-prozac folder in your
server directory.

This "fteprogs.dat" won't run on any other server than
our modified MVDSV. Download and compile the server
executable before trying to run this .DAT file. On any
other server it will crash if you try.

To use these progs file on our server just set the 
sv_progsname server cvar to "fteprogs". Alternatively if
you use the traditional "make.bat" the resulting progs
file will be called "qwprozac.dat" and the server defaults
to that progs filename.

+---------------------------------------------------------+
| Sergi Fumanya Grunwaldt, OfteN [cp] October 3rd of 2020 |
+---------------------------------------------------------+

Update January 11th of 2023 - Linux compiling support

Included binaries for compiling QuakeC, built on Debian 11.
You can use the "make.sh" to compile using FTEQcc OR -
Use "makecp.sh" to compile using original tools.
