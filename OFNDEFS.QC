/*=======================================================//
// ofndefs.QC - CustomTF 3.2.OfN           - 30/1/2001 - //
// by Sergio Fumaña Grunwaldt - OfteN [cp]               //
=========================================================//
 Debug flags and other *IMPORTANT* settings and flags
 --------------------------------------------------------
 Special number chars reference:  (why here? dunno..)

 0123456789  --> Normally white
 °±²³´µ¶·¸¹  --> Normally red
   --> Normally gold
   --> Normally gold (2nd variation)
  
 The actual color depends on the gfx.wad the client uses
 or the charset loaded into fuhquake.
=========================================================*/

// This file does not have any QuakeC, only preprocessor stuff, so..
#pragma DONT_COMPILE_THIS_FILE 

//================================================================================//
// MAIN BUILD CONFIGURATION  (un/comment any of the following lines to change it) //
//________________________________________________________________________________//

#define _RELEASE_BUILD_   // <-- ENABLE THIS FOR ANY BUILDS TO BE PLAYED!!
#define _FANCY_STUFF_     // <-- Turns on all fancy stuff, like cool gibs etc..
//#define DEBUG_CMD         // <-- Enable/Disable the debug command in cmnd.qc
//================================================================================//

//==============================//
// Debug/Test flags & enablers  //
//==============================//

//------------------------------------------//
#ifndef _RELEASE_BUILD_                     //
//------------------------------------------//
//#define VERBOSE_GRUNTY
//#define ARMYEXTRAS_TEST
///#define WARLOCK_TEST // free summon and all gibs collectable regardless of team
//#define ARMY_TEST // only 5 seconds for tele the soldier
//#define GIBMONSTERS_TEST // To test gibbing with a cmd
//#define CTF_FLAGS_TEST // Test what the EF_FLAGx do, what the fuck them do?
//#define BYTEPUT_TEST // To test .float as a parameter
//#define BLOODSPLASH_TEST
//#define CRUSADER_TEST
//#define ALIASES_TEST
//#define GRENADES_TEST // Infinite grenades
//#define HOLO_TEST //Sentries, monsters and teslas attack friendly holos?
//#define FIELD_TEST
#define VOTING_TEST
//#define FLOATING_TEST
//#define BACKPACK_N_AMMO_TEST // Ammo boxes and backpacks don't get removed
//#define XMAS_TEST
//#define CRASH_TEST
//#define STORM_TEST // Enables special admin cmnd "thunder"

//#define TESTRANGE_EXTRA
//#define SNIPERSIGHT_SHOWPOS
//#define MINE_DEBUG // to debug guerilla

//#define MONSTERS_AURA 2 // for testing 1 = power 2 = resis 5 = Regen
//#define MONSTERS_STATE #TFSTATE_INFECTED

//#define GRUNTY_EXTRA_WEAPONS #WEAP_ROCKET_LAUNCHER | #WEAP_SUPER_SHOTGUN // 0

// Attack owner for testing purposes?
//#define MAD_GRUNTY
//#define MONSTERS_GIBBED_EASY
//#define GIBRANDOM_TEST

//=== MAP DEBUGGING ===//
//#define MAP_DEBUG
//#define VERBOSE
//#define DEBUG_SPRITE
//=====================//

///#define MAD_MONSTERS
//#define MAD_MONSTERS_BETWEEN_THEM // Only attack between monsters

///#define MONSTERS_STARTINGLEVEL_DEBUG 1

///#define GIVEME_CMND

//#define MULTIFRAG_TEST // Use airstrike impulse to test multifrag

///#define ANGLESHOW_TEST // Show angle components on airstrike impulse

//#define GREMLIN_STARTING_WEAPS  #WEAP_SHOTGUN|#WEAP_SUPER_SHOTGUN|#WEAP_NAILGUN|#WEAP_SNG|#WEAP_LIGHTNING|#WEAP_ROCKET_LAUNCHER
//#define GREMLIN_AMMO_AMOUNT_TEST  20

//#define MONSTERS_TEST_HP 5 // Testing initial HP for easily killable to test death anims

//#define MAD_TESLA
///#define MAD_SENTRY
#define MAD_MINITURRET
#define VERBOSE_MINI_ANGLEX
//#define MINITURRET_NO_DMG
//#define SENTRY_AND_TESLA_NO_DMG // when this is enabled both sentry and tesla do no damage
#define MACHINES_ANYATTACK_IN // Makes buildings be damaged by teammates or owner too
//#define SPECTATOR_TEST
//#define HUNGRY_GREMLIN // adds a cmnd to trigger gorge of gremlins, for testing

//#define SETINFO_TEST // Enables the "cmd asetinfo Key Value" for testing

//#define CROWDY_TEST // Display volume of reactions
//#define SETFRAGS_CMD // Allows to set frags manually with a command
///#define PLASMAGUN_TEST // No cells consumption
///#define AIRSTRIKE_TEST // 10 seconds delay
//#define BIOMONSTER_TEST // Infects your shambler on airstrike impulse, used to test monsters infection

///#define MONSTERS_NODMG // Currently only affects to the shambler and scrag, makes monster not do damage
#define NO_DAMAGE_AT_ALL // Bypasses all damage done in tf_t_damage
//------------------------------------------//
#endif // !_RELEASE_BUILD_                  //
//------------------------------------------//

//===================================================//
// UNIMPLEMENTED/BUGGED STUFF DISABLERS              //
//===================================================//

//#define GREMLIN_NOTDONE // <-- Disables gremlin
//#define FISH_NOTDONE // <-- Disables piranha
//#define CAMLOOK_DISABLED // <-- Disables the camera looking feature
//#define PILOT_NOTDONE // <-- Disables pilot job, while developed
//#define MULTIJOB_NOTDONE // <-- Disables purchasing multiple jobs
#define DISABLE_MONSTERS_INFECTION // <-- Disables infection for monsters
#define CRAPPY_CALTROP_FIX // FIXME: this shouldn't be here, temporal fix until i understand why pqwsv screws up caltrops when we use a normal setsize...
#define SCRAG_IGNORE_BSP // Disables glitched into bsp check for scrags, as it's bugged
//#define DISABLE_SHAMBLERS_INFECTION // <-- Disables shamblers' infection

//===================================================//
// Dispensable visual/sound/gameplay stuff enablers  //
//===================================================//

//------------------------------------------//
#ifdef _FANCY_STUFF_                        //
//------------------------------------------//
#define KICKABLE_HEADS             // <-- Allows heads to be kickable
#define GIBABLE_CORPSES            // <-- Makes corpses gibable (shit breaks if not enabled right now)
#define COOL_GIBS                  // <-- Makes gibs pushable by explosions
#define COOL_GIBS_TRACE            // <-- Makes gibs pushable by trace attacks
#define COOL_METAL_GIBS            // <-- Enables the same for machine gibs
#define MENU_SOUNDS                // <-- Enables all menu sounds
#define SHAMBLER_SHOCK_SOUND       // <-- Electric sound for shambler lightning
#define EXTRA_DETPACK_EXPLOSIONS   // <-- Makes extra explosions for detpacks
#define TEAMITEMS_COLOURED         // <-- Gives color to team-specific tf items
#define IDABLE_CORPSES             // <-- Allows corpses to be ID'ed
#define IDABLE_TFITEMS             // <-- Allows tf items to be ID'ed
#define IDABLE_RUNES               // <-- Allows runes to be ID'ed
#define SCRAG_COLORED              // <-- Uses the team-skinned colored scrag model
#define CROWDY_VALUATION           // <-- Displays valuation on player quit 
#define CROWDY_SOUNDS              // <-- and plays crowdy reaction sample wav's (boo or ovation)
#define NEW_DETPACK_PRINTS         // <-- New colored text messages for detpacks
#define CLEAN_CPRINTS              // <-- Bypasses most CPrintX funcions not to interfere with custom menu or MOTD
#define AIRSTRIKE_DISPLAY_TIMELEFT // <-- Displays time left for next airstrike bombing raid, on timer set and impulse
#define STATUS_BAR_APPENDIX        // <-- Enables the status bar appendix, which shows extra information on sbar
#define MTEAM_CENTERPRINT_PREFIX   // <-- Shows the version prefix on menu team centerprint, ala MegaTF
#define MTEAM_CENTERPRINT_SUFFIX   // <-- Displays mapname and information after the team selection text
#define COOL_TRACE_ATTACKS         // <-- Gibs created from trace attacks get pushed from attacker's direction
#define MULTIFRAG_ENABLED          // <-- Enables the multifrag scoring sounds and messages, if defined
#define CARRY_FLAGS                // <-- Makes some TF flag items to appear like being carried by players
#define COLOURED_SENTRYFIRE        // <-- Lits sentries and teslas (and miniturrets) with color when firing 
#define COLOURED_MINEFLASH         // <-- Flash guerilla mines in color for activation
#define COLOURED_MINESMODEL        // <-- Enables a special skinned model for land mines
#define SPRITE_BINARY_HACKING      // <-- Enables the 1/0 binary towards device when hacking (thanks to OMC or whoever came up with the idea)
#define HACK_COLOR_SPRITE          // <-- Uses color sprites for hacking binary animations
#define CPRINT_FIXES               // <-- Definitive measures to erase Centerprint blinks once and for all
#define SENTRY_ANGLEX_AIM          // <-- Makes sentries to aim in the X axis (pitch) when facing enemy and firing
#define WARLOCK_EXTRAINFO          // <-- Counts stats for all monsters (non army) and displays it in the warlock monster menu
#define HEALTH_BARS                // <-- Displays health bars on warlock and army menus, for monsters and soldiers respectively
#define TEAM_FINALCROWD_REACTION   // <-- If defined players get a sample played depending if their team won or lost at matches ending
#define FLASH_SBAR_INFLICTED       // <-- If sbar appendix is enabled, flash with colored text the inflicted text item when reporting damage
#define STATUSBAR_TEAMSCORE_FLASH  // <-- Makes the status bar team scores flash when increased
#define MTEAM_CENTERPRINT_SUFFIX2  // <-- Extra suffix info on team selection menu
#define RADIOBUZZ_ENABLE           // <-- Precaches and uses radio buz noise for army and airforce jobs notices
#define SHAMBLER_COLORED           // <-- Usage of the shambler colored skins model
#define GREMLIN_COLORED            // <-- Usage of the gremlin colored skins model
#define FIEND_COLORED              // <-- Usage of the fiend colored skins model
#define FISH_COLORED               // <-- Usage of the fish colored skins model
#define PERSISTENT_BQ_NAMES        // <-- Enables makestr() calls to assign names to bodyque's corpses
#define COMMAND_CENTER             // <-- Enables the "Command Center" basically menus and features when looking through the camera
#define AGENT_SCORE_STUFF          // <-- Adds a damage dealt to enemy sorted list at the end of match, thanks to Agent for the idea! 
#define CLIENT_TEXTCOLORS          // <-- Disable this if you don't want the mod to use client coloured text features at all
#define RUNES_TEXTCOLOR            // <-- Enables if the previous item is enabled too, the purple colors on rune taken/drop
#define SUMMONS_TEXTCOLOR          // <-- Displays a shade of purple depending on level of summon, on summon act
#define ENABLE_CUSTOM_MENU_HELP    // <-- Allows users to enter "help-mode" on class customizing menus
#define COLOURED_LASERBOLT         // <-- Enables the use of team colored laser cannon bolt model
#define ORIENTED_SIGHTS            // <-- Uses and sets the "oriented" sniper sight sprite
#define DOG_INCLUDED               // <-- Enables compilation of the dog monster, ala megatf, although no megatf code was used for it
#define ZOMBIE_INCLUDED            // <-- Enables compilation, mostly for crucified ones.. (see zombie.qc)
#define AGENT_SCRAG_FALL           // <-- Makes a random alternate falling scrag death animation, thanks to agent, it's his work
#define BACKPACK_GRENADES          // <-- If this is enabled backpack's dropped by players may contain grenades
#define SMART_AUTOTEAM             // <-- Enables intelligent picking auto-team function (team with less frags gets priority when same players amount)
#define FLOATING_GIBS              // <-- Experimental, makes heads and gibs to float on water/liquids surface
#define OMC_TELE_MODEL             // <-- Uses the teleporter with skin when disabled (credit to OMC or whoever edited the mdl skins)
#define AAGUN_ENABLED              // <-- Enables the mega-like anti air turrets
//------------------------------------------//
#endif // _FANCY_STUFF_                     //
//------------------------------------------//

//==================================//
// Tweakable settings from here...  //
//==================================//

#define HOMING_MIS_DMG  108 // Radius damage for the homing missiles
#define HOMING_INFRONT_THRESHOLD 0.3 // Dot product result to be considered target-able by homing missiles firing

#define FARPIPES_RATE 0.8 // Rate of far pipes, attackfinished, Normal pipes check W_FireGrenade() call is 0.6

#define PYRO_FLAME_UPGRADED_SPEED_FACTOR   1.7 // Factor to be applied to upgraded pyro flame thrower flame speed - November 2024
#define ROCKETS_UPGRADED_SPEED_FACTOR      1.4 // Factor to be applied to upgraded rockets speed - November 2024

//#define ZG_GUN // Enables The Gizmo's ZERO Gravity gun

#define FLAME_DMG 19 // May 2024 - Original was 15

#define ASSAULTCANNON_ALLOWSMOVE
#define ASSAULTCANNON_SPEEDFAC 0.3

#define ORIENTED_SIGHT_OFFSET 0.1

#define STATUSBAR_CPRINT_ID_DELAY 2 // Controls the delay of id'ed centerprints - Original was 1.5

#define SHAMBLER_CONC_INMUNE // Makes shambler not to bounce on concussion grens explosions

#define FRAGFILE_FRIENDLY_OBITS // Excludes 2 death messages which couldn't be parsed in fuh's fragfile's system

#define MOTD_REFRESHRATE        1

#define STATUS_BAR_INFLICTED_RATE  2.1 // September 2020 or 1?

#define MCLASSHELP_PAGE1_TIME 18
#define MCLASSHELP_TOTAL_TIME 36

#define ALTERNATIVE_START_STATE

#ifdef ALTERNATIVE_START_STATE
#define PLAYER_START_MOVETYPE   #MOVETYPE_NONE // August 2020 was #MOVETYPE_FLY
#else
#define PLAYER_START_MOVETYPE   #MOVETYPE_NOCLIP//#MOVETYPE_FLY 
#endif

#define COOL_TRACEATTACK_MOVEFACTOR  12 // was 18 // was 10
#define COOL_TRACEATTACK_CHANCE       0.5 // Modded on April 2021 was 0.5 // 0.333 // 1 = always affected, 0 = never

#define SHOW_TIMELIMIT_CHANGES // I prefer this disabled and not publicly set timelimit

#define MOVETYPE_DEAD    #MOVETYPE_TOSS // Experiment with movetype when dead

#define RESPONSIVE_START // Makes first attack to get thru when showing class help at the start, first shot

#define EMP_DISCHARGE // Enables ancient code for EMP's

// Used for the Drop Ammo "cmd" function in cmnd.qc and cpstuff.qc OfN_DropAmmo()
#define AMMOTYPE_SHELLS   1
#define AMMOTYPE_NAILS    2
#define AMMOTYPE_ROCKETS  3
#define AMMOTYPE_CELLS    4

// Valuation definitions -----------------------------------------------------------------------//
#define VALUATION_MAXSCORE_FOR_BOO       -4 // Critical value, below or equal to this is "lame"
#define VALUATION_MINSCORE_FOR_BOO      -20 // Only taken to calc the max sample volume
#define VALUATION_MAXSCORE_FOR_OVATION  100 // Only taken to calc the max sample volume
#define VALUATION_MINSCORE_FOR_OVATION   50 // Critical value, above or equal to this is "cool"
#define CROWD_SND_VOLUME_MAX    3
#define CROWD_SND_VOLUME_MIN    2
#define CROWD_COOL_ABOVE       #VALUATION_MINSCORE_FOR_OVATION
#define CROWD_LAME_BELOW       #VALUATION_MAXSCORE_FOR_BOO
#define PLAYER_TIME_TO_MATTER  10 // Minimum minutes in-game for a player to get a comment on performance or any crowd reaction
//-----------------------------------------------------------------------------------------------//

#define GRUNTY_HP             800 // April 2021 set to 800 from 600 // initial HP for soldier
#define GRUNT_MAX_HP         1200 // OfN - Max health for the damn soldier, no 5000 hp grunts anymore! =)

/* moved to warlock.qc due to new hp system for monsters - Remains here as a reference to old values
#define SHAMBLER_MAX_HP          2600 // 2000 - 3000 - 2600
#define DEMON_MAX_HP             1800 // 1200 - 2000 - 1400
#define SCRAG_MAX_HP             1000 // 800
#define FISH_MAX_HP              1000 // was 850
#define GREMLIN_MAX_HP           2000*/

#define NAIL_DMG          16//March 2023 - 14 // Original quake is 12
#define SUPERNAIL_DMG     20//March 2023 - 17 // Original quake is 13

#define OTR_DMG_FACTOR        1.25 // July 2020 was 1.15 // Damage an otr bullet does respect a normal one
#define OTR_AMMO_COST         2 // Shells an OTR bullet takes when used

#define SCRAG_DMG              45 // October 2020 was 40 - damage the scrag does // was 29
#ifdef MAD_MONSTERS_BETWEEN_THEM
#define SCRAG_DMG 0 // For testing, scrags do no damage with this set
#endif

#define WAYPOINT_LIFE         240 // 120 after this amount of seconds any unused waypoint will be removed
#define MAXAMMO_CELLS         275 // was 320 // Engineer max is set on defs.qc PC_ENGINNER_XX
#define SENTRY_UNLOCKTIME       2.5 // (must be greater than 1.1) time for the sentries to begin to rotate after they have no target (+/- 1 second)
// Deprecated - #define HAX_UNSTABLEDIST      300 // distance at which enemy hacks take longer cause of "unstable connection"
#define WAYPOINT_AI_LIFE       10 // life for grunty ai created waypoints - last seen enemy mark
#define ATTN_MONSTERDIE         #ATTN_NONE // #ATTN_NORM
#define NOPHS_ADD_MONSTERDIE #CHAN_NO_PHS_ADD// March 2023
#define EXPBODY_DMG           160 // damage exp. body does /was 140

#define BERSERKER_HP_COST      40 // was 30 (i think 50 originally)

//----- Damage factor settings --------//
#define BERSERKER_DMGFACTOR        2.1 //2.4 //-was 2.65 was 2 then 2.2
#define CHAPLAN_DMGFACTOR          1.8 // Original was 1.5??
#define POWERAURA_DMGFACTOR        2
#define TRIADRUNE_DMGFACTOR        2 // March 2021 was 2.5

#define RESISAURA_DMGFACTOR        0.6
#define RESISRUNE_DMGFACTOR        0.5
#define CHAPLAN_DEFENSE_DMGFACTOR  0.66

#define MAX_OUT_DMGFACTOR          5
#define MIN_IN_DMGFACTOR           0.25
//-------------------------------------//

#define DISABLE_SAVEME2 // Disable the "speech/saveme2.wav" saveme sound, was weird and occupies space on sound precaches so...

#define ROCKET_SPEED		1200 // Original in CuTF = 900
#define ROCKET_DMG           108 // Original in CuTF = 92
#define NAIL_SPEED          1800 // May 2024 was 1600

#define LIGHTASSAULT_BULLETS   5// May 2024 was 4 // Original was 5

#define LASER_CANNON_DMG       36//26 // was 28

#define GRUNTY_SEEKTIME         8

#define BIO_GREN_RADIUS       200 // was 150

// Field generator
#define FIELDGEN_NUMCELLS    150 // Was 120
#define FIELDS_DONT_REACT_TOBUILDS

// Holograph 
#define HOLO_POWER_COST     10   //
#define HOLO_CYCLE_COST      2   //
#define HOLO_CYCLE_TIME      1.5 //

#define FLARE_DURATION      60   //

#define TEAM_MINE_REPORTS // Enabled July 2020

//#define CUSTOM_RESETS // Uncomment for non-persistence of custom class

#define ARMYSOLDIER_DEFAULT_NOTJUMP // Default army soldiers to stop instead of jump (June 2023)

// field generator ranges
#define FIELDGEN_RANGE          300 //285 // 275 - max distance between the two generators to work
#define FIELDGEN_HACKEDRANGE    475 // 450 - hacked generator distance
#define FIELDGEN_HACKEDRANGE2   650 // 600 - Range for both gens hacked

//#define FIELD_FORCEMODE    // coment to get rid of the mode switch of the force field
#define NO_FIELDGEN_INTERFERENCE

#define BUILD_DISPENSER_MAX_DETS         2

#define DISPENSER_MEDIKIT_MINIMUM        5 //15- Minimum medikit on dispenser to cure/heal player

#define TELEPORTER_TURRETIZE_CELLS_COST  80 // was 125

#define AURA_MONSTER_REGEN_FACTOR 2.5

// detpack fx
#ifdef EXTRA_DETPACK_EXPLOSIONS
    #define EXTRA_DETEXPLOSIONS       4 // 5 // was 6
#endif

#define DETPACK_EARTHQUAKE // <-- Shakes players near detpacks explosion

#ifdef DETPACK_EARTHQUAKE
    #define DETPACK_EARTHQUAKE_RANGE  1200
#endif

#define ADMIN_CMND_ALLOWED

#define CEASEFIRE_ALLOWS_TO_MOVE
#define CEASEFIRE_CPRINT_RATE   0.5

#define AIRFIST_DONT_PUSH_TEAMMATES
#define AIRFIST_DONT_HURT_TEAMMATES

#define AIRFIST_DMG_REDUCTION    0.7 // was 0.65
#define AIRFIST_RECOIL_REDUCTION 0.6
#define AIRFIST_PUSH_REDUCTION   0.5 // Only applies to players pushed

#define DONT_SET_AURA_TO_ENEMIES // unless they r spies, of course..
#define ALLOW_AURAS_ON_CRUSADERS

#define PUSHABLE_SCRAG // scrag pushable by the airfist?
#define PUSHABLE_ARMY
#define PUSHABLE_FIEND

#define HUNTED_YELLOWTEAM_FACTOR 0.5 // this multiplied to current red players is max for yellow team on huntedr
#define BORDER_FACTOR 0.5

#define SENTRY_ROCKETSND "weapons/spike2.wav"
//#define SENTRY_ROCKETSND "weapons/rocket1i.wav"

#define MAPCYCLE_SPEED 0.3 // Seconds, rate of map cycle advance (see client.qc)
#define CYCLE_TIMEOUT 1 // Amount of seconds to delay the check for error on map cycle (see client.qc)

#define GAS_GREN_CYCLES  9 // Original value = 3

#define MINHP_TOKILL_MONSTERS   300 // Minimum hp monsters/soldiers must have to be able to unsummon/detonate them
// Obsolete --> #define MAX_PLAYER_HP    150 // Maximum player hp allowed

// Gibs/Heads/Monster corpses duration settings (in seconds)
#define MIN_GIB_TIME    20
#define MAX_GIB_TIME    40
#define MIN_HEAD_TIME   30
#define MAX_HEAD_TIME   60
#define MIN_CORPSE_TIME 50 // Monster corpses only
#define MAX_CORPSE_TIME 70 // Monster corpses only

// Corpses damage tolerance (strength until they get gibbed)
#ifdef GIBABLE_CORPSES
#define CORPSE_HP_PLAYER   180//270//360//180
#define CORPSE_HP_SCRAG    140//210//280//140
#define CORPSE_HP_FISH     120//180//240//120
#define CORPSE_HP_DEMON    200//300//400//200
#define CORPSE_HP_GREMLIN  220//330//440//220
#define CORPSE_HP_SHAMBLER 340//510//680//340
#define CORPSE_HP_ARMY     #CORPSE_HP_PLAYER
#define CORPSE_HP_DOG      160
#endif

#define PLAYER_GIB_BELOWTHIS  -40 // Used to set ARMY soldiers gib threshold too. Monsters gib ones defined in "Warlock.qc"

// Realistic VelocityForDamage() constants/settings
#define MAX_VELFORDMG_FACTOR  2.4 //10 //2
#define MIN_VELFORDMG_FACTOR  1.25 //2 //0.5 // Original 0.7
#define MAX_VELFORDMG_HP      -50
#define MIN_VELFORDMG_HP      -140
// For low damage scenario (resulting hp more than -50)
#define MAXLOW_VELFORDMG_FACTOR 1.25
#define MINLOW_VELFORDMG_FACTOR 0.6
#define MAXLOW_VELFORDMG_HP   -1
#define MINLOW_VELFORDMG_HP   -50

// Blood splash settings
#define MAX_BLOODSPLASH            100
#define MIN_BLOODSPLASH             50
#define MAX_BLOODSPLASH_HP         -50
#define MIN_BLOODSPLASH_HP        -120
#define MAX_BLOODSPLASH_SPREAD     255//220
#define MIN_BLOODSPLASH_SPREAD      96//112
#define MAX_BLOODSPLASH_SPREAD_HP  -50
#define MIN_BLOODSPLASH_SPREAD_HP -120

#define REALISTIC_GIBCORPSE

#define MAX_CORPSE_BLOOD   15

// Kickable heads settings
#ifdef KICKABLE_HEADS
#define KICKHEAD_RATE           1
#define KICKHEAD_RATE_PERUSER   1.5
#define KICKHEAD_RATE_ALIVE     3
#endif

#define DEFAULT_BODYQUE_SIZE    12 // was 10 // Maximum player corpses/heads present in que (original was 4)

#define REVERSED_SBAR_SIZE // Tired of toggling the sbar size, so making 2 the default, see status.qc

#define FARPIPES_ADDSTRENGTH 400//500//650 // was 850 June 2022

#define BOLT_REALISTIC_RICOCHET // Enables a more realistic bolt (laser cannon and bolt-drones) ricochet calculation against surface normals

// Old stuff enablers (should be commented out normally)
//#define OLD_HP_SYSTEM
//#define OLD_FLASH // Warning: If this one is ever defined it should be in defs.qc or earlier, not here
//#define OLD_CONCS
//#define OLD_FLAME
//#define OLD_NAILGREN
//#define OLD_TEAM_ANNOUNCEMENTS
//#define OLD_SBARLOOK
//#define OLD_VELFORDMG
//#define OLD_GIBS_RANDOM // Original buggy but somehow realistic gib randomization on throwgib()

// Napalm grens
#define NAPALM_FIRSTDMG 40
#define NAPALM_MINRATE 0.1
#define NAPALM_MAXRATE 0.4
#define NAPALM_BURNTIMES 12 // May 2022 - was 8
#define NAPALM_MINDMG 60
#define NAPALM_MAXDMG 120
#define NAPALM_MAINDMG 140

#define AIRFIST_CELLS_COST    5

#define PSIONICS_TIME 4.5 // Seconds for the psionic gren effects to last
#define PSIONIC_MOVEFACTOR 0.5
#define PSIONIC_EXPLOSIONRANGE 250

#define SENTRIES_MINIMUMHP_TODETONATE_FACTOR 0.5

#define MINES_IGNORE_TEAMMATETOUCH
#define STANDARDMINES_DONTHURT_TEAMMATES // May 2021
#define MINE_MIRV_NUM 8 // May 2023 was 6
#define MINE_NAPALM_BURNTIMES 12
#define MINE_PSIONICS_TIME 6.5

#define EMPGREN_DISABLE_TIME 8.5 // 4.5
#define EMPMINE_DISABLE_TIME 8.5 // 6.5

#define NAILGREN_DMG  20 // <-- original value was 12

#define HACKER_ATTEMPTRATE 1

#define SENTRY_SMOOTHROTATION
#define SENTRY_ROTATIONSTEPS  36
#define SENTRY_ROTATIONWIDTH  50

#define INTERMISSION_LENFORCED  6 // 2020 June was 10 // Minimum seconds an intermission will last, Original was 5

#define SPEEDAURA_FACTOR 1.3

// Rune settings
#define SPEEDRUNE_FACTOR 1.35
#define SPEEDRUNE_RELOADMULT 0.6 // July 2020 was 0.8

#define FINALIZED_RATE_TEAMFLASH 1
#define FINALIZED_DELAY_TOACTION 40

#define MIN_MINUTES_TOVOTEMAP 1 // Minimum minutes to allow a map change using voting

#define TFITEM_SAFEDROP_TIME 2 // seconds for tfitems to not be pickeable by same player after tossing them

// Items/players color glowing settings
#ifdef TEAMITEMS_COLOURED
#define TEAMITEMS_COLORGLOW   // If defined, tf items only able to be picked by blue or red teams (like flags), will glow accordingly (only if 2 teams)
#define TEAMITEMS_PLAYERCOLOR // If defined, players will glow with corresponding color if picked a team restricted tf item (like flags) (only if 2 teams)
#define TEAMITEMS_FORCEDGLOW  // If TEAMITEMS_COLORGLOW and this are defined, team restricted items will always glow when dropped, even if the contrary was specified
#endif

// 2020 - backpacks
#define BACKPACKS_TIMEALIVE 30 // Seconds a backpack last on map after dropped, before getting removed - Hard-coded original was 20

//#define FIXES_FOR_FLOATINGBUILDS
#define REALISTIC_SHOTGUN_PARTS

#define SUICIDE_RESPAWN_DELAY 5 // 2020 June was 10 // If ppl complain, set it to 8 :P Original was 5

#define DEFAULT_CYCLEDIR cpcycle// Default was "qwmcycle"
#define UNABLE_TOVOTE_FORSAME_MAP

#define IGNORE_DISPENSERS_ONMENUS

// April 2022 - For color text on modern clients - what's considered maximum damage, so reddish
#define MAX_DETPACK_REPORTDMG 1200
#define MAX_AIRSTRIKE_REPORTDMG 1200
#define MAX_EMP_REPORTDMG 600
#define MAX_NAPALM_REPORTDMG 400
#define MAX_CRUSADER_REPORTDMG 500

#define TKD_DIVISOR 5//July 2022 was 3//4.5//3 // Original shaka is 12. Less division = more damage
#define TKD_NERF // April 2023

#define GREM_INITAMMO_FACT 0.75 //3/4 of initial load of ammo, for initialized gremlins

//#define XMAS_STUFF
//#define LIGHTNING_PUSHES

//__________________________________________________________//
//                                                          //
// Stuff from here are NOT settings and they are ESSENTIAL  //
//__________________________________________________________//

// String flags (used for fast find() searches)
#define STRFLAG_COOL               "cool"
#define STRFLAG_MONSTER            "XX"
#define STRFLAG_CORPSE             "corpse"
#define STRFLAG_STORM_TARGET       "strmtrgt"
#define STRFLAG_EARTHQUAKE_TARGET  "eqtarget"
#define STRFLAG_DUMMIE             "dume"
#define STRFLAG_MACHINE            "flgmchn" // April 2022

//------------------------------------------------------------------//
// DEFINED FIELDS - Method to easily change them using preprocessor //
//------------------------------------------------------------------//

#define runes     runes_owned 
#define mapflags  has_holo

#define monster_type  increase_team3
#define monster_level increase_team4

#define sname1 t_s_h
#define sname2 t_s_m
#define sname3 t_s_c

#define stormflag       n_s_m
#define earthquakeflag  n_s_h
#define coolflag        message
#define monsterflag     message
#define corpseflag      message
#define machineflag     message // April 2022

#define corpse_HP      if_group_is_removed

#define dummieflag     b_b

#define warlockgone    lastwalk

// APRIL 2022
#define dmg_dealt      volume
#define dmg_taken      distance

/*
THESE WERE the original fields, reused, but somehow conflicted with TF code so using dedicated fields now, see below
#define disabledstuff1 display_item_status1
#define disabledstuff2 display_item_status2
#define disabledstuff3 display_item_status3
#define disabledstuff4 display_item_status4
#define disabledstuff5 restore_group_no
#define givenstuff1    if_goal_is_active
#define givenstuff2    if_goal_is_inactive
#define givenstuff3    if_goal_is_removed
#define givenstuff4    if_group_is_active
#define givenstuff5    if_group_is_inactive*/

// Dedicated entity fields now!
#define disabledstuff1 dstuff1
#define disabledstuff2 dstuff2
#define disabledstuff3 dstuff3
#define disabledstuff4 dstuff4
#define disabledstuff5 dstuff5
#define givenstuff1    gstuff1
#define givenstuff2    gstuff2
#define givenstuff3    gstuff3
#define givenstuff4    gstuff4
#define givenstuff5    gstuff5

#define OfNTriggerMsg undercover_name

#define cprint_fx endtime

//==========================================//
// Flag values and important type values    //
//==========================================//

// OfN - Possible map bit flags on world.#mapflags field - 2020 some of them deprecated
#define MAPFLAG_WATERVISED  1 // works
#define MAPFLAG_NOTEAMSCORE 2 // works - Makes to ignore team score increase in normal player frags
#define MAPFLAG_GUARDOFF    4 // works - Disables spawn guard for the map
#define MAPFLAG_OBSERVER    8 // works - Allows observers to move
#define MAPFLAG_STORM       16 // still works
#define MAPFLAG_EARTHQUAKE  32 // still works
#define MAPFLAG_DAYTIME     64 // still works
#define MAPFLAG_RUNES       128 // still works
#define MAPFLAG_NOCUSTOM    256 // still works
#define MAPFLAG_NOSTOCK     512 // still works
#define MAPFLAG_NOEXTRAS    1024 // not sure, after search of the code, it maybe buggy
//#define MAPFLAG_STOCKFREE   2048 // dunno what this even was or was intended to become

// Client types - Mirror any changes with PQWSV source code!!
#define CLTYPE_DEFAULT     1
#define CLTYPE_PROZACQW    2
#define CLTYPE_FUHQUAKE    3
#define CLTYPE_AMFQUAKE    4
#define CLTYPE_ZQUAKE      5
#define CLTYPE_QUAKEFORGE  6
#define CLTYPE_FTEQUAKE    7
#define CLTYPE_EZQUAKE     8
#define CLTYPE_FORTRESSONE 9
#define CLTYPE_FO_FTE      10
#define CLTYPE_DARKPLACES  11

// Rune bit flags - DO NOT MODIFY
#define RUNE_TRIAD  1
#define RUNE_RESIS  2
#define RUNE_SPEED  4
#define RUNE_REGEN  8

// Aura types - DO NOT MODIFY
#define AURA_POWER	1
#define AURA_RESIS  2
#define AURA_HASTE	3
#define AURA_INVIS	4
#define AURA_REGEN  5
#define AURA_SPEED  6

// Gib Kilograms <-> gib models (they *MUST* be integer values)
#define GIB1_KGS 1 // this corresponds to the model: "progs/gib1.mdl" (the smallest one)
#define GIB2_KGS 3 // this corresponds to the model: "progs/gib2.mdl" (the biggest one)
#define GIB3_KGS 2 // this corresponds to the model: "progs/gib3.mdl" (the medium one)

// MegaTF mode flags
#define MEGATF_MAPENTS  1
#define MEGATF_STEPS    2
#define MEGATF_SIGHTS   4
#define MEGATF_SHELLS   8
#define MEGATF_SOUNDS  16
#define MEGATF_CARRY   32
#define MEGATF_PYRO    64
#define MEGATF_DROPS  128 // December 2023

#define MEGATF_WALK_RATE    0.275
#define ATTACK_RELOAD_RATE  0.275

// MegaTF style shell kinds
#define SHELLKIND_SHOTGUN    1
#define SHELLKIND_SSHOTGUN   2
#define SHELLKIND_ASSAULT    3
#define SHELLKIND_SNIPER     4
#define SHELLKIND_AUTORIFLE  5
#define SHELLKIND_SENTRY     6 // November 2021
#define SHELLKIND_MINITURRET 7 // November 2021
#define SHELLKIND_MTF_TURRET 8 // March 2022

#define MEGATF_SAMPLE_PRIMEGREN     1
#define MEGATF_SAMPLE_THROWGREN     2
#define MEGATF_SAMPLE_RELOAD        3
#define MEGATF_SAMPLE_ATTACKRELOAD  4
#define MEGATF_SAMPLE_GRENBOUNCE    5
#define MEGATF_SAMPLE_BLUELEADS     6
#define MEGATF_SAMPLE_REDLEADS      7
#define MEGATF_SAMPLE_TIED          8
#define MEGATF_SAMPLE_HUMILIATION   9
#define MEGATF_SAMPLE_STARTRELOAD  10

// May 2023 Timelimit changing kind of votes
#define VOTE_TIMELIMIT      5
#define VOTE_EXTEND         6
#define VOTE_SHRINK         7
// December 2023 - Misc votes
#define VOTE_MISC           8

// Server state flags DEPRECATED
/*#define SVSTATE_RAIN     1
#define SVSTATE_SNOW     2
#define SVSTATE_FOG      4
#define SVSTATE_HASLIT   8
#define SVSTATE_HASHRT   16
#define SVSTATE_HASSKY   32
#define SVSTATE_TEAM1POP 64
#define SVSTATE_TEAM2POP 64
#define SVSTATE_TEAM3POP 64
#define SVSTATE_TEAM4POP 64
*/

//====================================//
// Composed definitions               //
//====================================//

#ifdef GIBABLE_CORPSES
    #define USE_CORPSE_CODE
#endif

#ifdef IDABLE_CORPSES
    #ifndef USE_CORPSE_CODE
        #define USE_CORPSE_CODE
    #endif
#endif

#ifndef CROWDY_VALUATION
	#ifdef CROWDY_SOUNDS
		#undef CROWDY_SOUNDS
	#endif
#endif

#ifndef SPRITE_BINARY_HACKING
	#ifdef HACK_COLOR_SPRITE
		#undef HACK_COLOR_SPRITE
	#endif
#endif

#ifndef CLIENT_TEXTCOLORS
	#ifdef RUNES_TEXTCOLOR
		#undef RUNES_TEXTCOLOR
	#endif
	#ifdef SUMMONS_TEXTCOLOR
		#undef SUMMONS_TEXTCOLOR
	#endif
#endif

// November 2024
#ifdef CAMLOOK_DISABLED
	#ifdef COMMAND_CENTER
		#undef COMMAND_CENTER
	#endif
#endif