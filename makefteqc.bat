@echo off
echo ------------------------------------------------
echo This will compile Prozac CustomTF using [FTEQCC]
echo ------------------------------------------------
fteqcc.exe -srcfile FTEPROGS.SRC -Fhashonly -haltonfirst
if errorlevel 1 goto end
echo ------------------------------------------------
echo Prozac CustomTF has been compiled successfully!!
echo ------------------------------------------------
echo (Copying resulting progs file to parent dir...)
copy progs.dat ..\fteprogs.dat
:end