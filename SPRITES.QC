/*=======================================================//
// sprites.QC - CustomTF 3.2.OfN           - 18/4/2004 - //
// by Sergio Fuma�a Grunwaldt - OfteN [cp]               //
=========================================================//
 Sprites stuff
---------------------------------------------------------//
 Animation of sprites and functions related to them
=========================================================*/

// Sprite types
#define SPRITE_ABLAST     1
#define SPRITE_AIRBURST   2

// Sprite move/animation types
#define SPRITEMOVE_USEDEF 0
#define SPRITEMOVE_UP     1
#define SPRITEMOVE_UPSLOW 2
#define SPRITEMOVE_RNDORG 3

/*===============================================================================================

EXPLANATION OF HOW THE ENTITY FIELDS ARE USED (thnx? np.. :P) 
---------------------------------------------

For sprite entities:
--------------------

.has_fieldgen - Sprite animation rate

================================================================================================*/

// these are the only sprites still in the game...

/*$spritename s_explod
$type vp_parallel
$load /raid/quake/id1/gfx/sprites/explod03.lbm
$frame	24	24	56	56
$frame	120	24	56	56
$frame	216	24	56	56
$frame	24	88	56	56
$frame	120	88	56	56
$frame	216	88	56	56


$spritename s_bubble
$type vp_parallel
$load /raid/quake/id1/gfx/sprites/bubble.lbm
$frame	16	16	16	16
$frame	40	16	16	16


$spritename s_light
$type vp_parallel
$load /raid/quake/id1/gfx/sprites/light.lbm
$frame	104	32	32	32
*/

void() ab_exp1; // mtfents.qc 
void() run_ablast1; // AirFist.qc
float() crandom;
vector(vector org, vector zone) GetRandomOrigin;

entity(float num, float type, vector org, vector direction, float animtype, float animrate, void() CallbackFunc) SpawnSprite =
{
    local entity sprite, oself;
    local float counter;
	local vector originaldir, originalorg;

	originaldir = direction;
	originalorg = org;

    counter = 0;
    
    oself = self;

    while (counter < num)
    {
		sprite = spawn();

		sprite.movetype = #MOVETYPE_NOCLIP;
		sprite.solid = #SOLID_NOT;
		
		if (type == #SPRITE_ABLAST)
			setmodel(sprite, "progs/s_ablast.spr");
		else if (type == #SPRITE_AIRBURST)
			setmodel(sprite, "progs/s_explod.spr");
		
		if (animtype == #SPRITEMOVE_RNDORG)
			org = GetRandomOrigin(originalorg, originaldir);
		else
			org = originalorg;
					
		direction = originaldir;

		setorigin(sprite, org);

		if (animtype == #SPRITEMOVE_UP)
		{
			if (type == #SPRITE_ABLAST)
				direction_z = 240 + crandom()*100;
			else
				direction_z = 150 + crandom()*100;
			
			direction_x = crandom()*120;
			direction_y = crandom()*120;
		}
		else if (animtype == #SPRITEMOVE_UPSLOW)
		{
			direction_z = 50 + crandom()*50;
			direction_x = crandom()*20;
			direction_y = crandom()*20;
		}
		else if (animtype == #SPRITEMOVE_RNDORG)
		{
			direction_z = 25 + crandom()*25;
			direction_x = crandom()*80;
			direction_y = crandom()*80;
		}

		sprite.velocity = direction;

		if (animrate <= 0)
			sprite.has_fieldgen = 0.1;
		else
			sprite.has_fieldgen = animrate;

		self = sprite;
		CallbackFunc();

		if (type == #SPRITE_ABLAST)
			run_ablast1();
		else if (type == #SPRITE_AIRBURST)
			ab_exp1();
		else
		{
			sprite.think = SUB_Remove;
			sprite.nextthink = time + 0.1;
		}
	
        counter = counter + 1;
    }

    self = oself;

    return sprite;
};

//=====================================================
// Sets default rate on a sprite or custom rate

void() SetSpriteThink =
{
    if (!self.has_fieldgen)
        self.nextthink = time + 0.1;
    else
        self.nextthink = time + self.has_fieldgen;
};

//=========================================================================
// Gets a random origin within the zone boundaries of the specified vector

vector(vector org, vector zone) GetRandomOrigin =
{
	local vector result;

	result_x = org_x + zone_x*crandom();
	result_y = org_y + zone_y*crandom();
	result_z = org_z + zone_z*crandom();

	return result;
};