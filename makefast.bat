@echo off
echo ------------------------------------------------
echo This will preprocess and compile Prozac CustomTF
echo ------------------------------------------------
cppreqcc -fast
if errorlevel 1 goto end
cpqccx /O2 /fast
if errorlevel 1 goto end
echo ------------------------------------------------
echo Prozac CustomTF has been compiled successfully!!
echo ------------------------------------------------
:end
