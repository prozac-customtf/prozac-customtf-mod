/*=======================================================//
// MegaTF.QC - CustomTF 3.2.OfN            - 21/1/2004 - //
// by Sergio Fuma�a Grunwaldt - OfteN [cp]               //
=========================================================//
 Code related to MegaTF mimetized behaviours is here.
 With the exception of map entities, that have their own
 file: 'mtfents.qc'
=========================================================*/

// Release code --> // 
#define MEGATF_DEFAULT_MODE #MEGATF_MAPENTS | #MEGATF_SIGHTS | #MEGATF_SHELLS | #MEGATF_SOUNDS | #MEGATF_STEPS | #MEGATF_CARRY | #MEGATF_PYRO | #MEGATF_DROPS

//#define MEGATF_DEFAULT_MODE #MEGATF_MAPENTS | #MEGATF_SIGHTS | #MEGATF_SHELLS | #MEGATF_SOUNDS | #MEGATF_STEPS 

#define MEGATF_STEPS_MINVOLUME 0.4
#define MEGATF_STEPS_WATERFACT 0.32 // June 2023

#define DIFF_DECLARE_OWNAGE  120 // Difference on team scores to display an "ownage message"

#define ITEMCARRY_RATE_ORIGIN 0.02

// Function declarations
void() ShellTouch;
void(entity tflag, entity tplayer, vector vecForward, vector vecUp) TF_Flag_SetPosition;

//==============================================================
// Conditionally precaches needed stuff for current megatf mode

void () MegaTF_Precaches =
{
    if (megatf & #MEGATF_STEPS)
    {
        precache_sound("player/step1l.wav");
        precache_sound("player/step1r.wav");
        precache_sound("player/step2l.wav");
        precache_sound("player/step2r.wav");
    }

    if (megatf & #MEGATF_SIGHTS) // I did create this spr myself, but who cares, megatf's idea
        #ifdef ORIENTED_SIGHTS
		precache_model2("progs/colsight2.spr"); 
		#else
		precache_model2("progs/colsight.spr"); // Original code before July 2022
		#endif
    else // Standard sights
        precache_model2("progs/sight.spr");
    
	if (megatf & #MEGATF_SHELLS)
	{
		precache_model("progs/shell2.mdl");
		precache_model("progs/20mmcase.mdl");
		precache_sound("weapons/shell1.wav");
		precache_sound("weapons/shell2.wav");
	}

	if (megatf & #MEGATF_SOUNDS)
	{
		precache_sound("weapons/prime.wav");
		precache_sound("weapons/throw.wav");
		precache_sound("weapons/reload3.wav");
		precache_sound("weapons/r_clip.wav");
		precache_sound("weapons/no_ammo.wav");
		precache_sound("weapons/gbounce.wav");

		precache_sound("bleads.wav");
		precache_sound("rleads.wav");
		precache_sound("tied.wav");

		precache_sound("player/humiliation.wav");
	}

	if (megatf & #MEGATF_PYRO)
	{
		precache_model("progs/incenrkt.mdl");
		precache_sound("doors/airdoor2.wav");
	}

	if (megatf & #MEGATF_DROPS) // December 2023
	{
		precache_model("progs/spikebal.mdl"); // Spike trap model
		precache_sound("doors/meduse.wav"); // Spike set
		precache_sound("shambler/smack.wav"); // Hits
		
		precache_model2("progs/v_spike.mdl"); // Toaster model
		precache_sound("misc/power.wav"); // Toaster fires
	}
};

//==============================================================
// Updates MegaTF flags global

void () GetMegaTFflags =
{
    local string st;

    st = infokey(world,"megatf");
    if (st == "")
        megatf = #MEGATF_DEFAULT_MODE;
    else
        megatf = stof(st);    
};

//===============================================================
// Called only at map startup, initializes megatf stuff

void() InitMegaTF =
{
    GetMegaTFflags();
    MegaTF_Precaches();
};

//======================================================================================
// Spawns a shell entity of the specified kind, for players or sentries/miniturrets

void(entity ent, float shellkind) PopShell =
{
	// First of all, abort if shells are not enabled
	if (!(megatf & #MEGATF_SHELLS))
		return;

	newmis = spawn();
	newmis.classname = "shell";
	newmis.movetype = #MOVETYPE_BOUNCE;
	newmis.solid = #SOLID_TRIGGER;
	newmis.owner = ent;

	if (ent.classname == "player")
	{
		makevectors(ent.v_angle);

		newmis.origin = ent.origin + v_forward *32 + v_right * 16 + v_up* 12;

		if (pointcontents(newmis.origin) == #CONTENT_SOLID)
		{                                           //23
			newmis.origin = newmis.origin - v_forward*16 - v_right*8; 

			if (pointcontents(newmis.origin) == #CONTENT_SOLID)
			{                                           //11
				newmis.origin = newmis.origin - v_forward*7 - v_right*4;
			}
		}

		newmis.nextthink = time + 2;
	}
	else if (ent.flags & #FL_MACHINE)
	{
		makevectors(ent.angles);

		if (ent.classname == "building_sentrygun")
		{
			local vector vr, vu;
			if (ent.tf_items & #NIT_TURRET)
			{
				if (ent.weapon == 1)
					vr = v_right;
				else
					vr = '0 0 0' - v_right;
				
				vu = '0 0 0';
			}
			else
			{
				vr = v_right;
				vu = v_up;
			}
			
			if (ent.weapon == 1) // Level 1 sentry
				newmis.origin = ent.origin + vr * 9 + vu * 32;
			else
			{ // Level 2 and 3 sentries
				if (!ent.option) // Alternate sides
					newmis.origin = ent.origin - vr * 16 + vu * 32;
				else
					newmis.origin = ent.origin + vr * 16 + vu * 32;

				ent.option = !ent.option; // set next the other side
			}

			newmis.nextthink = RandomFutureRange(0.35, 0.65);
		}
		else if (ent.classname == "building_miniturret")
		{
			newmis.origin = ent.origin + v_forward *7 - v_right * 6 + v_up* 12;
			newmis.nextthink = RandomFutureRange(0.2, 0.35);
		}
	}
	// March 2022
	else if (ent.classname == "monster_turret")
	{
		makevectors(ent.angles);
		
		if (!ent.option) // Alternate sides
			newmis.origin = ent.origin - v_right * 12;// + v_up * 32;
		else
			newmis.origin = ent.origin + v_right * 12;// + v_up * 32;

		ent.option = !ent.option; // set next the other side

		newmis.nextthink = RandomFutureRange(0.3, 0.5);
	}
	// End March 2022
	else
	{
		//LAGS SERVER, Just in case... //RPrint("Bug: Unknown entity type on PopShell()!\n");
		dremove(newmis);
		return;
	}

	setorigin(newmis, newmis.origin);
	setsize(newmis,'0 0 0','0 0 0');

	newmis.angles = vectoangles(v_forward);

	if (shellkind == #SHELLKIND_ASSAULT)
		setmodel(newmis, "progs/20mmcase.mdl");
	else
		setmodel(newmis, "progs/shell2.mdl");

	newmis.touch = ShellTouch;
	newmis.think = SUB_Remove;	
	
	newmis.velocity = v_right*80+ v_up*30 +v_right*20*crandom() +v_up*10*crandom();
	newmis.avelocity = '300 600 0' * crandom();

	if (shellkind == #SHELLKIND_SHOTGUN)
	{
		newmis.skin = 0;
	}
	else if (shellkind == #SHELLKIND_SSHOTGUN)
	{
		newmis.skin = 0;
		newmis.velocity = v_right*80+ v_up*30 + v_right*40*crandom() + v_up*15*crandom();
	}
	else if (shellkind == #SHELLKIND_ASSAULT)
	{
		newmis.nextthink = time + 0.6;
		newmis.velocity = v_right*80+ v_up*30 + v_right*10*crandom() + v_up*5*crandom();
	}
	else if (shellkind == #SHELLKIND_SNIPER)
	{
		newmis.skin = 1;
	}
	else if (shellkind == #SHELLKIND_AUTORIFLE)
	{
		newmis.skin = 1;
		newmis.nextthink = time + 0.8;
		newmis.avelocity = '200 200 200' * crandom(); 
	}
	else if (shellkind == #SHELLKIND_SENTRY)
	{
		newmis.skin = 1;
		newmis.touch = SUB_Null;

		local vector v_rights, vup;

		if (ent.tf_items & #NIT_TURRET)
		{
			vup = '0 0 0' - v_up;

			if (ent.option)
				v_rights = v_right;
			else
			{			
				if (ent.weapon == 1)
					v_rights = v_right;
				else
					v_rights = '0 0 0' - v_right;
			}
		}
		else
		{
			if (!ent.option)
				v_rights = v_right;
			else
				v_rights = '0 0 0' - v_right;			
		}

		newmis.velocity = v_rights*70 + vup*40 +v_rights*20*crandom() + vup*8*crandom();
		newmis.avelocity = '300 600 0' * crandom();
	}
	else if (shellkind == #SHELLKIND_MINITURRET)
	{
		newmis.skin = 0;
		newmis.touch = SUB_Null;

		local vector v_rights;
		v_rights = '0 0 0' - v_right;

		newmis.velocity = v_rights*80+ v_up*30 + v_rights*10*crandom() + v_up*5*crandom();
		newmis.avelocity = '150 300 0' * crandom();
	}
	// March 2022
	else if (shellkind == #SHELLKIND_MTF_TURRET)
	{
		newmis.skin = 1;
		newmis.touch = SUB_Null;

		local vector v_rights;
		
		if (!ent.option)
			v_rights = v_right;
		else
			v_rights = '0 0 0' - v_right;
		
		newmis.velocity = v_rights*60+ v_up*35 + v_rights*20*crandom() + v_up*6*crandom();
		newmis.avelocity = '150 300 0' * crandom();
	}
};

//=====================================================================================
// Touch function of shells, just make the bouncing sound

void() ShellTouch =
{
	if (CheckForPlatMove()) // June 2023 - Reusing this, was conceived for grenades
		return;

	if (other == world)
	{
		if (random() < 0.5)
			sound(self,#CHAN_BODY,"weapons/shell1.wav",1, #ATTN_STATIC);
		else
			sound(self,#CHAN_BODY,"weapons/shell2.wav",1, #ATTN_STATIC);
	}
};

//=======================================================================================
// Miscellanious megatf introduced sound samples

void(entity player, float channel, float samplekind, float svolume, float attenuation) PlayMegaSample =
{
	if (!(megatf & #MEGATF_SOUNDS))
		return;	

// Sound/player/headshot.wav
// weapons/no_ammo-wav
// misc/update,wav (when team scores)
// weapons/r_clip.wav
// Weapons/rocklr1a.wav

	if (samplekind == #MEGATF_SAMPLE_PRIMEGREN)
		sound(player,#CHAN_AUTO,"weapons/prime.wav",1,#ATTN_IDLE);
	else if (samplekind == #MEGATF_SAMPLE_THROWGREN)
		sound(player,#CHAN_BODY,"weapons/throw.wav",1,#ATTN_IDLE);
	else if (samplekind == #MEGATF_SAMPLE_RELOAD)
		sound(player,#CHAN_AUTO,"weapons/reload3.wav",1,#ATTN_IDLE);
	else if (samplekind == #MEGATF_SAMPLE_STARTRELOAD)
		sound(player,#CHAN_AUTO,"weapons/r_clip.wav",1,#ATTN_IDLE);
	else if (samplekind == #MEGATF_SAMPLE_ATTACKRELOAD)
		sound(player,#CHAN_AUTO,"weapons/no_ammo.wav",1,#ATTN_IDLE);
	else if (samplekind == #MEGATF_SAMPLE_GRENBOUNCE)
		sound(player,#CHAN_AUTO,"weapons/gbounce.wav",1,#ATTN_NORM);
	else if (samplekind == #MEGATF_SAMPLE_BLUELEADS)
		BroadcastSoundEx("bleads.wav",1);
	else if (samplekind == #MEGATF_SAMPLE_REDLEADS)
		BroadcastSoundEx("rleads.wav",1);
	else if (samplekind == #MEGATF_SAMPLE_TIED)
		BroadcastSoundEx("tied.wav",1);
	else if (samplekind == #MEGATF_SAMPLE_HUMILIATION)
		BroadcastSoundEx("player/humiliation.wav",1);
};

//=================================================================================================
// Walk step sounds

void (entity player, float side) MegaPlayStep =
{
	if (!(megatf & #MEGATF_STEPS))
		return;

	if (player.cutf_items & #CUTF_STEALTH)
		return;

	if (intermission_running)
		return;

	// November 2020
	if (player.waterlevel > 1)
		return;

	local float rvolume, fspeed;

	rvolume = 1 - ((1 - #MEGATF_STEPS_MINVOLUME)* random());

	local vector planespeed;

	planespeed = player.velocity;
	planespeed_z = 0;

	fspeed = vlen(planespeed);

	rvolume = rvolume* (fspeed/player.maxspeed);

	//if (rvolume < 0)
	//	rvolume = 0.01; // Was 0.04
	//else if (rvolume > 1)
	//	rvolume = 1;

	rvolume = bound(0.01, rvolume, 1);
	
	if (player.waterlevel == 1)
	{
		rvolume = rvolume * #MEGATF_STEPS_WATERFACT;

		//if (random() < 0.5)
			sound(player,#CHAN_AUTO,"player/h2ojump.wav",rvolume,#ATTN_IDLE);
		//else
		//	sound(player,#CHAN_AUTO,"player/inh2o.wav",rvolume,#ATTN_IDLE);
		return;
	}

	if (!side) // Left foot
	{
		if (random() < 0.5)
			sound(player,#CHAN_AUTO,"player/step1l.wav",rvolume,#ATTN_IDLE);
		else
			sound(player,#CHAN_AUTO,"player/step2l.wav",rvolume,#ATTN_IDLE);
	}
	else // Right foot
	{
		if (random() < 0.5)
			sound(player,#CHAN_AUTO,"player/step1r.wav",rvolume,#ATTN_IDLE);
		else
			sound(player,#CHAN_AUTO,"player/step2r.wav",rvolume,#ATTN_IDLE);
	}
};

//==================================================================================================
// Called when a single team scores just after displaying the scores

void(float teamthatscores) EventWhoLeads =
{
	if (number_of_teams == 2)
	{
		local float t1score, t2score, leadsby;
		local string t1name, t2name, tstr;

		t1name = TeamFortress_TeamGetColorString(1);
		t2name = TeamFortress_TeamGetColorString(2);
		t1score = TeamFortress_TeamGetScore(1);
		t2score = TeamFortress_TeamGetScore(2);

		if (t1score > t2score)
			leadsby = t1score - t2score;
		else
			leadsby = t2score - t1score;

		tstr = ftos(leadsby);
		tstr = colstr(tstr,#COLSTR_NUMBER);

		t1name = colstr(t1name,#COLSTR_RED);
		t2name = colstr(t2name,#COLSTR_RED);

		if (t1score == t2score)
		{
			tstr = ftos(t1score);
			tstr = colstr(tstr,#COLSTR_NUMBER);
			bprint(#PRINT_MEDIUM, "Game is tied at �",tstr,"� ������!\n");
			PlayMegaSample(world,#CHAN_AUTO,#MEGATF_SAMPLE_TIED,1,#ATTN_NONE);
		}
		else if (t1score > t2score)
		{
			if (leadsby >= #DIFF_DECLARE_OWNAGE && teamthatscores == 1 && !((t1score/10) & 1))
			{
				bprint(#PRINT_MEDIUM, "The �",t1name,"� team is owning the game, ahead by ",tstr," ������!\n");
				PlayMegaSample(world,#CHAN_AUTO,#MEGATF_SAMPLE_HUMILIATION,1,#ATTN_NONE);
				return;
			}
			else
			{
				bprint(#PRINT_MEDIUM,"The �",t1name,"� team leads by ",tstr," ������!\n");
			}
			
			if (teamthatscores == 1)
				PlayMegaSample(world,#CHAN_AUTO,#MEGATF_SAMPLE_BLUELEADS,1,#ATTN_NONE);
		}
		else if (t1score < t2score)
		{
			if (leadsby >= #DIFF_DECLARE_OWNAGE && teamthatscores == 2 && !((t2score/10) & 1))
			{
				bprint(#PRINT_MEDIUM, "The �",t2name,"� team is owning the game, ahead by ",tstr," ������!\n");
				PlayMegaSample(world,#CHAN_AUTO,#MEGATF_SAMPLE_HUMILIATION,1,#ATTN_NONE);
				return;
			}
			else
			{
				bprint(#PRINT_MEDIUM,"The �", t2name,"� team leads by ",tstr," ������!\n");
			}

			if (teamthatscores == 2)
				PlayMegaSample(world,#CHAN_AUTO,#MEGATF_SAMPLE_REDLEADS,1,#ATTN_NONE);
		}
	}
};

//============================================================================================//
// LIST OF SUPPORTED FLAG MODELS - PLAYER CARRY ==============================================//
//============================================================================================//

// Supported flag models:
#define FLAGMODEL_NOTSUPPORTED    0
#define FLAGMODEL_TF_FLAG         1
#define FLAGMODEL_HK_FLAG         2
#define FLAGMODEL_TF_STAN         3
#define FLAGMODEL_TF_TFC          4
#define FLAGMODEL_2MA_FLAG        5 // from 2macha/2machap map

// Position and heights relative to player
#define FLAGMODEL_TF_FLAG_POS     26 // 24
#define FLAGMODEL_TF_FLAG_HEIGHT  24

#define FLAGMODEL_HK_FLAG_POS     18
#define FLAGMODEL_HK_FLAG_HEIGHT  16

#define FLAGMODEL_TF_STAN_POS     16
#define FLAGMODEL_TF_STAN_HEIGHT  16

#define FLAGMODEL_TF_TFC_POS      16
#define FLAGMODEL_TF_TFC_HEIGHT   16

#define FLAGMODEL_2MA_FLAG_POS    14//13
#define FLAGMODEL_2MA_FLAG_HEIGHT 9//10

//===================================================================================================
// Returns the type of flag if the tfitem uses a carry-able flag model

float(entity tfitem) IsCarryFlag =
{
	if (!(megatf & #MEGATF_CARRY))
		return 0;
	
	if (tfitem.mdl == "progs/tf_flag.mdl")
		return #FLAGMODEL_TF_FLAG;
	if (tfitem.mdl == "progs/hk_flag.mdl")
		return #FLAGMODEL_HK_FLAG;
	if (tfitem.mdl == "progs/tf_stan.mdl")
		return #FLAGMODEL_TF_STAN;
	if (tfitem.mdl == "progs/tfc_flag_r.mdl")
		return #FLAGMODEL_TF_TFC;
	if (tfitem.mdl == "progs/flag.mdl")
		return #FLAGMODEL_2MA_FLAG;

	return #FLAGMODEL_NOTSUPPORTED;
};

//=========================================================================================
// Think routine for flag items while being carried

void() TF_Item_carry_think =
{
	makevectors(self.owner.angles);
	
	TF_Flag_SetPosition(self, self.owner, v_forward, v_up);
	
	local float tfloat;

	self.angles = self.owner.angles;
	tfloat = self.angles_x;
	self.angles_y = anglemod(self.angles_y + 90);
	self.angles_x = anglemod(0 - (self.angles_z));
	self.angles_z = anglemod(0 - tfloat);
	
	setorigin(self,self.origin); // NEEDED? if not here flag model sometimes disappear on some angles of the viewer
	self.nextthink = time + #ITEMCARRY_RATE_ORIGIN;
};

//=======================================================================================
// Sets the relative position/origin next to the player model for the given flag

void(entity tflag, entity tplayer, vector vecForward, vector vecUp) TF_Flag_SetPosition =
{
	#define DOWN_FULL_ANGLES     (0 - 26.7)
	#define UP_FULL_ANGLES       23.3

	local float FlagHeight;
	local float FlagPos;

	if (tflag.material == #FLAGMODEL_TF_FLAG)
	{
		FlagPos = #FLAGMODEL_TF_FLAG_POS;
		FlagHeight = #FLAGMODEL_TF_FLAG_HEIGHT;
	}
	else if (tflag.material == #FLAGMODEL_HK_FLAG)
	{
		FlagPos = #FLAGMODEL_HK_FLAG_POS;
		FlagHeight = #FLAGMODEL_HK_FLAG_HEIGHT;
	}
	else if (tflag.material == #FLAGMODEL_TF_STAN)
	{
		FlagPos = #FLAGMODEL_TF_STAN_POS;
		FlagHeight = #FLAGMODEL_TF_STAN_HEIGHT;
	}
	else if (tflag.material == #FLAGMODEL_TF_TFC)
	{
		FlagPos = #FLAGMODEL_TF_TFC_POS;
		FlagHeight = #FLAGMODEL_TF_TFC_HEIGHT;
	}	
	else if (tflag.material == #FLAGMODEL_2MA_FLAG)
	{
		FlagPos = #FLAGMODEL_2MA_FLAG_POS;
		FlagHeight = #FLAGMODEL_2MA_FLAG_HEIGHT;
	}	
	
	tflag.origin = tplayer.origin - vecForward * FlagPos;
	
	local float intens;
	
	if (tplayer.angles_x < 0)
	{
		intens = tplayer.angles_x/#DOWN_FULL_ANGLES;
		tflag.origin = tflag.origin - (vecUp*10)*(intens);
		
		tflag.origin_z = tflag.origin_z + FlagHeight*intens;			
	}
	else
	{
		intens = tplayer.angles_x/#UP_FULL_ANGLES;
		tflag.origin = tflag.origin - (vecUp*18)*(intens); // was 20 // was 14

		tflag.origin = tflag.origin - (vecForward * 8 * intens);
	}

	tflag.origin_z = tflag.origin_z + FlagHeight;
};

//=============================================================//
// MegaTF-Style drops (December 2023)
//=============================================================//

// defined in DEFS.QC        .float mtf_drops; // First 3 bits, spike trap bitwise flags, 4th to 6th bits toasters 

// Flags, not settings!
#define SPIKETRAP1_SET  1
#define SPIKETRAP2_SET  2
#define SPIKETRAP3_SET  4
#define TOASTER1_SET    8
#define TOASTER2_SET    16
#define TOASTER3_SET    32
#define STICKY1_SET     64// December 2024
#define STICKY2_SET     128// December 2024
#define STICKY3_SET     256// December 2024

#define dropnum has_sentry

// Settings
#define SPIKETRAP_DURATION      300 // Seconds for the spike traps to last
#define STICKY_DURATION         300 // Seconds for the sticky caltrops to last
#define TOASTER_PRIME_TIME        2 // Seconds for discharge after throw
#define TOASTER_REGEN_TIME      200 // Seconds to replenish an used toaster
#define TOASTER_RANGE          1000
#define TOASTER_DMG             400
#define MAX_TOASTER_REPORTDMG  1200 // Only visual for report, considered lot of damage
#define SPIKETRAP_DMG            60 // Original was 10
#define STICKY_DMG               50 // Damage for the caltrop sticky

// DEBUG FLAG - COMMENT OUT FOR ANY RELEASE
//#define DROPS_TEST

//====================================================================================================
// Get the available spike trap drops

float(entity player) GetNoAvailableSpikeTraps =
{
	if (!(player.cutf2_items & #CUTF2_DROPS))
		return 0;

	local float numspike;

	if (player.tf_items & #NIT_AMMO_BACKPACK)
		numspike = 3;
	else
		numspike = 2;

	if (player.mtf_drops & #SPIKETRAP1_SET)
		numspike = numspike - 1;
	if (player.mtf_drops & #SPIKETRAP2_SET)
		numspike = numspike - 1;
	if (player.mtf_drops & #SPIKETRAP3_SET)
		numspike = numspike - 1;

	return numspike;	
};

//====================================================================================================
// Get the available toasters

float(entity player) GetNoAvailableToasters =
{
	if (!(player.cutf2_items & #CUTF2_DROPS))
		return 0;

	local float numtoasters;

	if (player.tf_items & #NIT_AMMO_BACKPACK)
		numtoasters = 3;
	else
		numtoasters = 2;

	if (player.mtf_drops & #TOASTER1_SET)
		numtoasters = numtoasters - 1;
	if (player.mtf_drops & #TOASTER2_SET)
		numtoasters = numtoasters - 1;
	if (player.mtf_drops & #TOASTER3_SET)
		numtoasters = numtoasters - 1;

	return numtoasters;	
};

//====================================================================================================
// Get the available sticky caltrops

float(entity player) GetNoAvailableSticky =
{
	if (!(player.cutf2_items & #CUTF2_DROPS))
		return 0;

	local float numsticky;

	if (player.tf_items & #NIT_AMMO_BACKPACK)
		numsticky = 3;
	else
		numsticky = 2;

	if (player.mtf_drops & #STICKY1_SET)
		numsticky = numsticky - 1;
	if (player.mtf_drops & #STICKY2_SET)
		numsticky = numsticky - 1;
	if (player.mtf_drops & #STICKY3_SET)
		numsticky = numsticky - 1;

	return numsticky;	
};

//===========================================================================
// Spike traps - Drop1

void() SpikeTrap_touch;
void() SpikeTrap_think;

void() Player_Drop1 =
{
	if (!(megatf & #MEGATF_DROPS))
		return;

	#ifndef DROPS_TEST
	if (!(self.cutf2_items & #CUTF2_DROPS))
		return;
	#endif

	if (self.classname != "player")
		return;

	if (self.health <= 0)
		return;

	if (self.done_custom & #CUSTOM_BUILDING || self.playerclass == #PC_UNDEFINED)
		return;

	if (ceasefire)
		return;
	
	local float bits;
	bits = self.mtf_drops;

	if (GetNoAvailableSpikeTraps(self) <= 0)
	{
		sprint(self,#PRINT_HIGH,"You cannot set more spike traps!\n"); // Should silently fail and not report?
		return;
	}

	if (!(self.flags & #FL_ONGROUND))
		return;

	local entity spiketrap;

    spiketrap = spawn();
    spiketrap.team_no = self.team_no;
	#ifdef DROPS_TEST
    spiketrap.real_owner = self;
	spiketrap.owner = world;
	#else
	spiketrap.owner = self;
	#endif
    spiketrap.classname = "spiketrap";
    spiketrap.origin = (self.origin - '0 0 10');

	spiketrap.angles_x = 90;
    spiketrap.health = 5;
	spiketrap.touch = SpikeTrap_touch;
	spiketrap.think = SpikeTrap_think;
	spiketrap.nextthink = time + #SPIKETRAP_DURATION;

	spiketrap.angles_y = 45;
    spiketrap.flags = #FL_ITEM;
    spiketrap.solid = #SOLID_BBOX;
    spiketrap.movetype = #MOVETYPE_TOSS;
    spiketrap.velocity = '0 0 1';
	setmodel (spiketrap, "progs/spikebal.mdl");
    setsize (spiketrap, '-1 -1 0', '1 1 6');

	spiketrap.has_tesla = #FALSE;

	if (!(bits & #SPIKETRAP1_SET)) // First not set
	{
		spiketrap.#dropnum = #SPIKETRAP1_SET;
		bits = bits | #SPIKETRAP1_SET;
	}
	else if (!(bits & #SPIKETRAP2_SET))// Second spike trap not set
	{
		spiketrap.#dropnum = #SPIKETRAP2_SET;
		bits = bits | #SPIKETRAP2_SET;
	}
	else // Third spike trap not set
	{
		spiketrap.#dropnum = #SPIKETRAP3_SET;
		bits = bits | #SPIKETRAP3_SET;
	}

	sprint (self, #PRINT_HIGH, "You set a spike trap!\n");
	sound (self, #CHAN_AUTO, "doors/meduse.wav", #ATTN_NORM, 1);

	self.mtf_drops = self.mtf_drops | bits;

	spiketrap.pausetime = time + 2;
};

//=====================================================================================
// Touch function for the spike traps

void(entity trap, entity victim, float minmeat, float maxmeat, vector offset) SpawnSpiketrapMeatSpray;

void() SpikeTrap_touch =
{
	if (self.pausetime > time)
		return;

	if (other.classname != "player")
		return;

	if (other.health <= 0)
		return;

	if (other.done_custom & #CUSTOM_BUILDING || other.playerclass == #PC_UNDEFINED)
		return;

	if (ceasefire)
		return;

	#ifndef DROPS_TEST
	if (Teammate(other.team_no, self.team_no))
		return;
	
	#else

	self.owner = self.real_owner;
	#endif	

	sound (self, #CHAN_AUTO, "shambler/smack.wav", #ATTN_NORM, 1);
    sprint (other, #PRINT_HIGH, "You stepped on a spike trap!\n");
    sprint (self.owner, #PRINT_HIGH, "Your spike trap worked!\n");
    self.pausetime = time + 6;
    self.nextthink = time + 4;
	self.has_tesla = #TRUE;

	if (other.armorclass & #AT_SAVEELECTRICITY)
		HurtLegs(other, 1);
	else
		HurtLegs(other, 4);

    deathmsg = #DMSG_SPIKETRAP;
    TF_T_Damage (other, self, self.owner, #SPIKETRAP_DMG, #TF_TD_DIRECT | #TF_TD_DONTGIB, #TF_TD_NAIL);

	SpawnSpiketrapMeatSpray(self, other, 2, 3, '0 0 6.1');
};

//============================================================================================
// Spawns a randomized amount of meat sprays on spike trap when a victim walks on it

void(entity trap, entity victim, float minmeat, float maxmeat, vector offset) SpawnSpiketrapMeatSpray =
{
	local vector dir;
	local float counter, num_meat, rnd_duration;

	num_meat = RandomRange(minmeat, maxmeat);
	num_meat = rint(num_meat);

	counter = 0;

	while (counter < num_meat)
	{
		dir_x = crandom()* 200;
		dir_y = crandom()* 200;
		dir_z = random()* 70 + 30;

		rnd_duration = RandomRange(0.8, 1.4);

		//SpawnMeatSprayEx(trap.origin + '0 0 6.1', victim, dir, rnd_duration);
		SpawnMeatSprayEx(trap.origin + offset, victim, dir, rnd_duration);

		counter = counter + 1;
	}
};

//====================================================================================
// Think subroutine for the spike traps

void() SpikeTrap_think =
{
	self.owner.mtf_drops = self.owner.mtf_drops - (self.owner.mtf_drops & self.#dropnum);
	
	if (!self.has_tesla)
		sprint (self.owner, #PRINT_HIGH, "Your spike trap is gone...\n");
	
	dremove(self);
};

//===========================================================================
// Toasters - Drop2

void() Toaster_touch;
void() Toaster_think;

void() Player_Drop2 =
{
	if (!(megatf & #MEGATF_DROPS))
		return;

	#ifndef DROPS_TEST
	if (!(self.cutf2_items & #CUTF2_DROPS))
		return;
	#endif

	if (self.classname != "player")
		return;

	if (self.health <= 0)
		return;

	if (self.done_custom & #CUSTOM_BUILDING || self.playerclass == #PC_UNDEFINED)
		return;

	if (ceasefire)
		return;

	local float bits;
	bits = self.mtf_drops;

	if (GetNoAvailableToasters(self) <= 0)
	{
		sprint(self,#PRINT_HIGH,"You cannot launch more toasters right now!\n"); // Should silently fail and not report?
		return;
	}

	local entity toaster;

	makevectors(self.v_angle);

	toaster = spawn();
	toaster.classname = "toaster";
	toaster.owner = self;
	toaster.movetype = #MOVETYPE_BOUNCE;
	toaster.takedamage = #DAMAGE_NO;
	toaster.solid = #SOLID_BBOX;
	setmodel(toaster, "progs/v_spike.mdl");
	setsize(toaster, '0 0 0', '0 0 0');

	setorigin(toaster, self.origin);

	toaster.angles = vectoangles(v_forward);
	toaster.avelocity = '225 225 225';

	toaster.velocity = v_forward * 500 + '0 0 90';

	toaster.think = Toaster_think;
	toaster.nextthink = time + #TOASTER_PRIME_TIME;
	toaster.touch = Toaster_touch;
	toaster.has_tesla = #FALSE;

	if (!(bits & #TOASTER1_SET)) // First not launched
	{
		toaster.#dropnum = #TOASTER1_SET;
		bits = bits | #TOASTER1_SET;
	}
	else if (!(bits & #TOASTER2_SET))// Second toaster not launched
	{
		toaster.#dropnum = #TOASTER2_SET;
		bits = bits | #TOASTER2_SET;
	}
	else // third toaster not launched
	{
		toaster.#dropnum = #TOASTER3_SET;
		bits = bits | #TOASTER3_SET;
	}

	self.mtf_drops = self.mtf_drops | bits;
	PlayMegaSample(self,#CHAN_BODY,#MEGATF_SAMPLE_THROWGREN,1,#ATTN_IDLE);
};

//===========================================================================================
// Touch function for toasters

void() Toaster_touch =
{
	if (CheckForPlatMove())
		return;

	sound (self, #CHAN_WEAPON, "weapons/bounce.wav", 1, #ATTN_NORM);
};

//=====================================================================================
// Think sub-routine for toasters handles both discharge and timing for regen/resuply

void() Toaster_dmgReport;

void() Toaster_think =
{
	if (!self.has_tesla) // Discharge
	{
		self.has_tesla = #TRUE;
		self.nextthink = time + #TOASTER_REGEN_TIME;

		local float conts;
		conts = pointcontents(self.origin);

		if (conts == #CONTENT_WATER || conts == #CONTENT_SLIME || conts == #CONTENT_LAVA)
		{
			local entity te;
			local float dmgdone;

			dmgdone = 0;

			te = findradius(self.origin, #TOASTER_RANGE);

			sound(self, #CHAN_AUTO, "misc/power.wav", #ATTN_NORM, 1);

			while (te)
			{
				if (te.waterlevel > 0)
				if (te.classname == "player")
				if (te.takedamage)
				if (te.health > 0)
				if (te.radsuit_finished < time && !(te.tfstate & #TFSTATE_RADSUIT) && !(te.tf_items & #NIT_SCUBA))
				if (!(te.done_custom & #CUSTOM_BUILDING))
				if (te.playerclass != #PC_UNDEFINED)
				{
					deathmsg = #DMSG_TOASTER;
					dmgdone = dmgdone + TF_T_Damage(te, self, self.owner, #TOASTER_DMG, #TF_TD_DIRECT | #TF_TD_NOCURSE | #TF_TD_RETURNDMG, #TF_TD_ELECTRICITY);
				}

				te = te.chain;
			}

			// Spawn the damage report entity
			te = spawn();
			te.dmg = dmgdone;
			te.think = Toaster_dmgReport;
			EntNextRange(te, 0.4, 0.8);
			te.owner = self.owner;
		} // else TODO: make fail sound?

		// Make it disappear, will only act as a timer to restore the toaster slot...
		self.movetype = #MOVETYPE_NONE;
		self.solid = #SOLID_NOT;
		self.touch = SUB_Null;
		self.velocity = '0 0 0';
		self.avelocity = '0 0 0';
		self.modelindex = modelindex_null;
	}
	else // Removal
	{
		self.owner.mtf_drops = self.owner.mtf_drops - (self.owner.mtf_drops & self.#dropnum);
		dremove(self);
	}
};

//=======================================================================================
// Reports toaster damage

void() Toaster_dmgReport =
{
	if (self.owner.classname != "player" || !self.owner.is_connected)
	{
		dremove(self);
		return;
	}

	local float tf;
	local string ts;
	
	tf = self.dmg / #MAX_TOASTER_REPORTDMG;

	ts = ftos(floor(self.dmg));
	if (tf == 0)
		ts = "no";

	sprint(self.owner,#PRINT_HIGH,"Your toaster inflicted ");
	StartEffectColorText(self.owner,#PRINT_HIGH, tf);
	sprint(self.owner,#PRINT_HIGH, ts, " damage");
	EndColoredText(self.owner,#PRINT_HIGH);
	sprint(self.owner,#PRINT_HIGH,"!\n");

	dremove(self);
};

//===========================================================================================================
// Sticky caltrops

void() Sticky_think;
void() Sticky_touch;

void() Player_Drop3 =
{
	if (!(megatf & #MEGATF_DROPS))
		return;

	#ifndef DROPS_TEST
	if (!(self.cutf2_items & #CUTF2_DROPS))
		return;
	#endif

	if (self.classname != "player")
		return;

	if (self.health <= 0)
		return;

	if (self.done_custom & #CUSTOM_BUILDING || self.playerclass == #PC_UNDEFINED)
		return;

	if (ceasefire)
		return;

	local float bits;
	bits = self.mtf_drops;

	if (GetNoAvailableSticky(self) <= 0)
	{
		sprint(self,#PRINT_HIGH,"You cannot set more sticky caltrops right now!\n"); // Should silently fail and not report?
		return;
	}

	local entity sticky;

	makevectors(self.v_angle);

	traceline(self.origin,self.origin + v_forward * 50, #TL_BSP_ONLY, self);

	if (trace_fraction == 1 || trace_plane_normal_z == 1)
	{
		sprint(self,#PRINT_HIGH,"You must be close to a wall to set the sticky!\n");
		return;
	}

	sticky = spawn();
	sticky.team_no = self.team_no;
	sticky.classname = "sticky";
	sticky.owner = self;
	sticky.movetype = #MOVETYPE_NONE;
	sticky.takedamage = #DAMAGE_NO;
	sticky.solid = #SOLID_TRIGGER;
	setmodel(sticky, "progs/caltrop.mdl");
	setsize(sticky, '-10 -10 -10', '10 10 10'); // Too big?

	sticky.angles = vectoangles('0 0 0' - trace_plane_normal);

	setorigin(sticky, trace_endpos);

	sticky.think = Sticky_think;
	sticky.nextthink = time + #STICKY_DURATION;
	sticky.touch = Sticky_touch;
	sticky.has_tesla = #FALSE;

	if (!(bits & #STICKY1_SET)) // First not set
	{
		sticky.#dropnum = #STICKY1_SET;
		bits = bits | #STICKY1_SET;
	}
	else if (!(bits & #STICKY2_SET))// Second sticky not set
	{
		sticky.#dropnum = #STICKY2_SET;
		bits = bits | #STICKY2_SET;
	}
	else // third sticky not set
	{
		sticky.#dropnum = #STICKY3_SET;
		bits = bits | #STICKY3_SET;
	}

	self.mtf_drops = self.mtf_drops | bits;

	sprint (self, #PRINT_HIGH, "You set a sticky caltrop!\n");
	sound (self, #CHAN_AUTO, "doors/meduse.wav", #ATTN_NORM, 1);
};

void() Sticky_think =
{
	if (!self.has_tesla)
		sprint(self.owner,#PRINT_HIGH,"Your sticky caltrop is gone...\n");

	self.owner.mtf_drops = self.owner.mtf_drops - (self.owner.mtf_drops & self.#dropnum);
	dremove(self);
};

void() Sticky_touch =
{
	if (other.classname != "player")
		return;

	if (other.done_custom & #CUSTOM_BUILDING)
		return;

	if (other.playerclass == #PC_UNDEFINED)
		return;

	if (!other.takedamage)
		return;

	#ifndef DROPS_TEST
	if (Teammate(other.team_no, self.owner.team_no))
		return;
	#endif

	deathmsg = #DMSG_STICKY;

	sound (self, #CHAN_AUTO, "shambler/smack.wav", #ATTN_NORM, 1);
    sprint (other, #PRINT_HIGH, "You got hurt by a caltrop sticky!\n");
    sprint (self.owner, #PRINT_HIGH, "Your sticky caltrop worked!\n");

	SpawnBloodEx(other.origin, 1, 10);

	TF_T_Damage(other, self, self.owner, #STICKY_DMG, #TF_TD_DIRECT, #TF_TD_NAIL);

	SpawnSpiketrapMeatSpray(self, other, 2, 3, '0 0 0');
	
	self.owner.mtf_drops = self.owner.mtf_drops - (self.owner.mtf_drops & self.#dropnum);
	dremove(self);
};

//===================================================================================
// Clears any drops for the player

void(entity player) RemoveDrops =
{
	local entity te;

	te = find(world, classname, "spiketrap");

	while (te)
	{
		if (te.owner == player)
		{
			te.nextthink = time + 0.05;
			te.touch = SUB_Null;
			te.think = SUB_Remove;
		}

		te = find(te, classname, "spiketrap");
	}

	te = find(world, classname, "toaster");

	while (te)
	{
		if (te.owner == player)
		{
			te.nextthink = time + 0.05;
			te.think = SUB_Remove;
		}

		te = find(te, classname, "toaster");
	}

	te = find(world, classname, "sticky");

	while (te)
	{
		if (te.owner == player)
		{
			te.nextthink = time + 0.05;
			te.touch = SUB_Null;
			te.think = SUB_Remove;
		}

		te = find(te, classname, "sticky");
	}

	// reset drops' flags on us too...
	player.mtf_drops = 0;
};

