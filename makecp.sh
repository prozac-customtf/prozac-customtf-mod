#!/bin/bash

echo ------------------------------------------------
echo This will preprocess and compile Prozac CustomTF
echo ------------------------------------------------

./cppreqcc.bin

if [ $? -eq 0 ]
then
	./cpqccx.bin /O2 /w
else
	exit 1
fi

if [ $? -eq 0 ]
then
	echo ------------------------------------------------
	echo Prozac CustomTF has been compiled successfully!!
	echo ------------------------------------------------
else
	exit 1
fi

exit 0

